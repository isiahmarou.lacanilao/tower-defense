#include "BuildManager.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerPawn.h"
#include "TowerDefenseController.h"
#include "TowerBase.h"
#include "GhostTower_Base.h"
#include "BuildablePlatform_Base.h"
#include "Components/WidgetComponent.h"
#include "Widget_TowerMenu.h"

ABuildManager::ABuildManager()
{
	PrimaryActorTick.bCanEverTick = true;

}

void ABuildManager::BeginPlay()
{
	Super::BeginPlay();
	
	if (UWorld* world = GetWorld())
	{
		_PlayerPawn = Cast<APlayerPawn>(UGameplayStatics::GetPlayerPawn(world, 0));
		_Controller = Cast<ATowerDefenseController>(_PlayerPawn->GetController());

		_Controller->setBuildManager(this);
		
		TowerMenuWidget = Cast<UWidget_TowerMenu>(CreateWidget(_Controller, ClassTowerMenu));
		
		TowerMenuWidget->setPlayer(_PlayerPawn);
		TowerMenuWidget->setBuildManager(this);
		
		TowerMenuWidget->AddToViewport();
		TowerMenuWidget->SetVisibility(ESlateVisibility::Hidden);
	}
}

void ABuildManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

bool ABuildManager::OnBuildMode(class UTowerDataAsset* mTowerData)
{
	if (mTowerData->LevelProperties[0].Cost <= _PlayerPawn->getGoldAmount())
	{
		isInBuildMode = true;
		TowerToBuildData = mTowerData;

		if (UWorld* world = GetWorld())
		{
			if (TowerToBuildData->GhostTowerClass)
			{
				isInBuildMode = true;

				if (currentGhostTower) currentGhostTower->Destroy();

				currentGhostTower = world->SpawnActor<AGhostTower_Base>
					(TowerToBuildData->GhostTowerClass);

				if (currentGhostTower)
				{
					FTimerDelegate _Delegate = FTimerDelegate::CreateUObject(_Controller, &ATowerDefenseController::GhostFollowMouse, currentGhostTower);

					world->GetTimerManager().SetTimer(ChangeLocationHandle, _Delegate, 0.1f, true);
					return true;
				}
			}
		}
	}
	OnBuildModeEnd();
	return false;
}

void ABuildManager::OnBuildModeEnd()
{
	if (currentGhostTower) currentGhostTower->Destroy();

	TowerToBuildData =  nullptr;

	GetWorldTimerManager().ClearTimer(ChangeLocationHandle);

	isInBuildMode = false;
	BuildModeEnd.Broadcast(this);
}

void ABuildManager::BuildTowerOnPlatform(class ABuildablePlatform_Base* platform)
{
	if(!platform) return;

	if (isInBuildMode)
	{
		if (TowerToBuildData->TowerClass)
		{
			if (UWorld* world = GetWorld())
			{
				FActorSpawnParameters SpawnParameters;
				SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

				FVector loc = platform->GetActorLocation();
				FRotator rot = platform->GetActorRotation();
				
				ATowerBase* Tower = world->SpawnActor<ATowerBase>
					(TowerToBuildData->TowerClass, loc, rot, SpawnParameters);

				platform->setTowerOnPlatform(Tower);
				TowersBuilt.Add(Tower);
				
				_PlayerPawn->subtractGoldAmount(TowerToBuildData->LevelProperties[0].Cost);
				OnBuildModeEnd();
			}
		}
	}
}

void ABuildManager::OnTowerUpgrade(ATowerBase* tower)
{
	UE_LOG(LogTemp, Warning, TEXT("BUTTON: %f"),GetLastRenderTime() );
	if(!tower->IsMaxLevel())
	{
		int i = tower->getLevel();
		int cost =  tower->getDataAsset()->LevelProperties[i].Cost;

		if(cost > _PlayerPawn->getGoldAmount()) return;

		_PlayerPawn->subtractGoldAmount(cost);
		tower->OnUpgrade();
	}
}

void ABuildManager::SellTower(ATowerBase* tower)
{
	if(!tower) return;

	_PlayerPawn->addGoldAmount(tower->getSellValue());

	if(sellSFX)
	{
		UGameplayStatics::PlaySound2D(GetWorld(),sellSFX,.2f);
	}
	tower->Destroy();
}

void ABuildManager::ShowTowerWidget(ATowerBase* tower)
{
	TowerMenuWidget->setTower(tower);
	TowerMenuWidget->SetVisibility(ESlateVisibility::Visible);
}
