// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower_SW_Nanobot.h"

#include "Buff_Nanobot.h"
#include "HealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TowerDefenseGameModeBase.h"
#include "Components/SphereComponent.h"

ATower_SW_Nanobot::ATower_SW_Nanobot()
{
	
}

void ATower_SW_Nanobot::BeginPlay()
{
	ATower_SuperWeaponBase::BeginPlay();
	
	if(UWorld* world = GetWorld())
	{
		GameMode = Cast<ATowerDefenseGameModeBase>(UGameplayStatics::GetGameMode(world));
	}

	const FTimerDelegate delegate = FTimerDelegate::CreateUObject
				(this, &ATower_SW_Nanobot::OnSkillActivate);

	GetWorld()->GetTimerManager().SetTimer(BehaviorTimer, delegate,0.5f,true);
}

void ATower_SW_Nanobot::UpgradeStats()
{
	int i = Level-1;
	BehaviorInterval = DataAsset->LevelProperties[i].Interval;
	BehaviorValue = DataAsset->LevelProperties[i].Value;
	ZoneSphere->SetSphereRadius(DataAsset->LevelProperties[i].RadiusRange);
	
	Description.Empty();
	Description.Append("Grants the Nanobot passive skill" LINE_TERMINATOR);
	Description.Append("Automatically kills enemy below " + FString::SanitizeFloat(BehaviorValue*100) +"% HP" + LINE_TERMINATOR);
}

void ATower_SW_Nanobot::DamageNearbyEnemies(AEnemyBase* enemy)
{
	ZoneSphere->SetWorldLocation(enemy->GetActorLocation());
	
	const float radius = DataAsset->LevelProperties[Level-1].RadiusRange;
	ZoneSphere->SetSphereRadius(radius);

	TArray<AActor*> actors;
	ZoneSphere->GetOverlappingActors(actors);

	float damage = enemy->getEnemyHealthComponent()->getMaxHealth() * BehaviorValue;
	for(AActor* a : actors)
	{
		if(AEnemyBase* enemyToDamage = Cast<AEnemyBase>(a))
		{
			if(enemy != enemyToDamage)
			{
				enemyToDamage->TakeDamage(damage,FDamageEvent(),nullptr,nullptr);
			}
		}
	}
}

void ATower_SW_Nanobot::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	TArray<AEnemyBase*> enemies;
	GameMode->getEnemies(enemies);

	for(AEnemyBase* enemy : enemies)
	{
		if(UBuff_Nanobot* slow_Component = Cast<UBuff_Nanobot>(enemy->GetComponentByClass(UBuff_Nanobot::StaticClass())))
		{
			slow_Component->DestroyComponent();
		}
	}
}

void ATower_SW_Nanobot::OnSkillActivate()
{
	TArray<AEnemyBase*> enemies;
	GameMode->getEnemies(enemies);

	for(AEnemyBase* enemy : enemies)
	{
		if(!enemy->GetComponentByClass(UBuff_Nanobot::StaticClass()))
		{
			UBuff_Nanobot* slow_Component = NewObject<UBuff_Nanobot>(enemy);
			slow_Component->setBuffValue(BehaviorValue);
			enemy->AddOwnedComponent(slow_Component);
			slow_Component->RegisterComponent();
			slow_Component->OnEnemyKill.AddDynamic(this, &ATower_SW_Nanobot::DamageNearbyEnemies);
		}
	}
}
