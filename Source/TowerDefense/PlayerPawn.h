// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Base_CharacterPawn.h"
#include "PlayerPawn.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API APlayerPawn : public ABase_CharacterPawn
{
	GENERATED_BODY()
public:
	APlayerPawn();
protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USpringArmComponent* SpringArm;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UFloatingPawnMovement* PlayerMovement;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 GoldAmount;

	UPROPERTY()
	class ATowerDefenseController* mController;

	UPROPERTY()
	class UHealthComponent* playerHealthComponent;

	virtual void OnDeath() override;

public :
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
	void addGoldAmount(int32 additionalGold);

	UFUNCTION(BlueprintCallable)
	void subtractGoldAmount(int32 value);

	UFUNCTION(BlueprintCallable)
	FORCEINLINE UCameraComponent* getCamera() { return Camera; }

	UFUNCTION(BlueprintCallable)
	FORCEINLINE USpringArmComponent* getSpringArm() { return SpringArm; }

	UFUNCTION(BlueprintPure, BlueprintCallable)
	FORCEINLINE UHealthComponent* getPlayerHealthComponent() { return playerHealthComponent; }

	UFUNCTION(BlueprintCallable)
	int32 getGoldAmount();


};
