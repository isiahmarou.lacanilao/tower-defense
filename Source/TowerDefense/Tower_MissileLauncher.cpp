// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower_MissileLauncher.h"
#include "Components/SphereComponent.h"
#include "EnemyBase.h"
#include "Components/ArrowComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "ProjectileBase.h"

ATower_MissileLauncher::ATower_MissileLauncher()
{
	Pivot_ = CreateDefaultSubobject<USceneComponent>(TEXT("Pivot"));
	GunMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GunMesh"));
	Muzzle_ = CreateDefaultSubobject<UArrowComponent>("Muzzle");
	
	Pivot_->SetupAttachment(RootComponent);
	GunMesh->SetupAttachment(Pivot_);
	Muzzle_->SetupAttachment(GunMesh);
}

void ATower_MissileLauncher::BeginPlay()
{
	ATowerBase::BeginPlay();

	const FTimerDelegate delegate = FTimerDelegate::CreateUObject
				(this, &ATower_MissileLauncher::TowerBehavior);

	GetWorld()->GetTimerManager().SetTimer(BehaviorTimer, delegate,BehaviorInterval,true,0.1f);
}

void ATower_MissileLauncher::TowerBehavior()
{
	LookForTarget(currentTarget,false);
	
	if (IsValid(currentTarget))
	{
		FHitResult hit = AimAndShootAtEnemy(currentTarget ,Pivot_,Muzzle_, MissileClass);

		if(MissileLaunchSFX)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(),MissileLaunchSFX,this->GetActorLocation(),this->GetActorRotation(), .2f);
		}
		
		FTimerHandle ImpactHandle;
		FTimerDelegate ImpactDelegate = FTimerDelegate::CreateUObject
				(this, &ATower_MissileLauncher::MissileImpact, hit);

		GetWorld()->GetTimerManager().SetTimer(ImpactHandle, ImpactDelegate,0.6,false,0.6);
	}
	else
	{
		Pivot_->SetRelativeRotation(FRotator(0.f,0.f,0.f));
	}
}

FHitResult ATower_MissileLauncher::AimAndShootAtEnemy(AEnemyBase* Target, USceneComponent* Pivot, UArrowComponent* Muzzle, TSubclassOf<class AProjectileBase> ProjectileClass)
{
	// aim at Target
	FVector Offset = FVector(0,0,+50);
	FVector targetLoc =  Target->GetPawnViewLocation() + Offset;
	FRotator lookAtRot = UKismetMathLibrary::FindLookAtRotation(Pivot->GetComponentLocation(), targetLoc);
	Pivot->SetWorldRotation(lookAtRot);

	//predict 
	FHitResult hit;
	TArray<FVector> path;
	FVector lastDestination;
	FVector start = Muzzle->GetComponentLocation();
	FVector velocity = Muzzle->GetForwardVector() * 1000.f;
	TArray<AActor*> actorsToIgnore;
	actorsToIgnore.Add(this);
	UGameplayStatics::Blueprint_PredictProjectilePath_ByTraceChannel(GetWorld(), hit, path, lastDestination, start, velocity,
		 true, 10.f, ECC_Visibility, true, actorsToIgnore,
		EDrawDebugTrace::None, 5.f, 10,2);

	// Spawn Projectile
	if(UWorld* world = GetWorld())
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	
		world->SpawnActor<AProjectileBase>(ProjectileClass, Muzzle->GetComponentTransform(), SpawnParameters);
	}
	
	return hit;
}

void ATower_MissileLauncher::MissileImpact(FHitResult OutHit)
{
	if(OutHit.bBlockingHit)
	{
		if(explosionSFX)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(),explosionSFX,this->GetActorLocation(),this->GetActorRotation(), 0.4f);
		}
		
		// explosion damage
		USphereComponent* explosionSphere = NewObject<USphereComponent>(this, USphereComponent::StaticClass());
		explosionSphere->RegisterComponent();
			
		if(explosionSphere)
		{
			explosionSphere->SetWorldLocation(OutHit.Location);
			explosionSphere->SetSphereRadius(explosionRadius);
			
			TArray<AActor*> overlappingActors;
			explosionSphere->GetOverlappingActors(overlappingActors);
		
			for(int i =0; i < overlappingActors.Num(); i++)
			{
				if(AEnemyBase* enemy = Cast<AEnemyBase>(overlappingActors[i]))
				{
					if(!enemy->getIsFlying())
					{
						enemy->TakeDamage(BehaviorValue, FDamageEvent(), nullptr, this);
					}
				}
			}
			explosionSphere->DestroyComponent();
		}
	}
}

void ATower_MissileLauncher::UpgradeStats()
{
	int i = Level-1;
	BehaviorInterval = DataAsset->LevelProperties[i].Interval;
	BehaviorValue = DataAsset->LevelProperties[i].Value;
	explosionRadius = DataAsset->LevelProperties[i].RadiusRange * 0.6;
	ZoneSphere->SetSphereRadius(DataAsset->LevelProperties[i].RadiusRange);

	
	Description.Empty();
	Description.Append("Explosion Damage: " + FString::SanitizeFloat(BehaviorValue) + LINE_TERMINATOR);
	Description.Append("Explosion Radius: " + FString::SanitizeFloat(explosionRadius) + " Units" + LINE_TERMINATOR);
	Description.Append("Attack Speed: per "+FString::SanitizeFloat(BehaviorInterval) + " second" + LINE_TERMINATOR);
	Description.Append("Attack Radius: " + FString::SanitizeFloat(ZoneSphere->GetScaledSphereRadius())+ " Units");
}

void ATower_MissileLauncher::UpgradeMesh()
{
	ATowerBase::UpgradeMesh();

	int i = Level-1;
	if(i <= DataAsset->LevelProperties.Num())
	{
		GunMesh->SetStaticMesh(DataAsset->LevelProperties[i].GunMesh);
	}
}
