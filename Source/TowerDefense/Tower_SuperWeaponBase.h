// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TowerBase.h"
#include "Tower_SuperWeaponBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCoolDown, TSubclassOf<class ATower_SuperWeaponBase>, TowerClass);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnTowerSold, TSubclassOf<class ATower_SuperWeaponBase>, TowerClass);
UCLASS()
class TOWERDEFENSE_API ATower_SuperWeaponBase : public ATowerBase
{
	GENERATED_BODY()
public:
	ATower_SuperWeaponBase();
protected:
	virtual void BeginPlay() override;

	UPROPERTY()
	float CountDown;
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> WidgetClass;
	
	class UWidget_SkillUI* SkillUI;

	UFUNCTION()
	void OnCountDown();

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
public:
	UFUNCTION(BlueprintPure,BlueprintCallable)
	FORCEINLINE bool IsOnCoolDown() { return CountDown <= 0; }

	UFUNCTION(BlueprintPure,BlueprintCallable)
	FORCEINLINE float getCountDownPercentage() { return CountDown/BehaviorInterval; }
	
	UFUNCTION(BlueprintCallable)
	virtual void OnSkillActivate();

	UPROPERTY(BlueprintAssignable, category = "EventDispatchers")
	FOnCoolDown CoolDownStarts;

	UPROPERTY(BlueprintAssignable, category = "EventDispatchers")
	FOnCoolDown CoolDownEnds;

	UPROPERTY(BlueprintAssignable, category = "EventDispatchers")
	FOnTowerSold TowerSold;
};
