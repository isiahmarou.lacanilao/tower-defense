// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower_SuperWeaponBase.h"

#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Widget_SkillUI.h"

ATower_SuperWeaponBase::ATower_SuperWeaponBase()
{
	
}

void ATower_SuperWeaponBase::BeginPlay()
{
	ATowerBase::BeginPlay();
	
	TArray<UUserWidget*> widgets;
	UWidgetBlueprintLibrary::GetAllWidgetsOfClass(GetWorld(),widgets,WidgetClass);
    	
	if(widgets.Num()>0)
	{
		
		SkillUI = Cast<UWidget_SkillUI>(widgets[0]);
		SkillUI->InitBTN(this);
	}
}

void ATower_SuperWeaponBase::OnCountDown()
{
	CountDown--;
	
	if(CountDown<=0)
	{
		CoolDownEnds.Broadcast(this->GetClass());
		GetWorldTimerManager().ClearTimer(BehaviorTimer);

		if(GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Cool Down Ends"));
		}  

	}
}

void ATower_SuperWeaponBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	TowerSold.Broadcast(this->GetClass());
}

void ATower_SuperWeaponBase::OnSkillActivate()
{
	
}
