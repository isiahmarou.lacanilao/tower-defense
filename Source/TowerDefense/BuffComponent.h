// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BuffComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOWERDEFENSE_API UBuffComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBuffComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Value;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Interval;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int BuffTicks;
	
	UFUNCTION()
	virtual void BuffBehavior();

	UPROPERTY(BlueprintReadOnly);
	AActor* owner;
	
	UPROPERTY(BlueprintReadWrite)
	FTimerHandle BuffTimerHandle;
	
	int currentTick;
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void RefreshBuff();

	UFUNCTION(BlueprintCallable)
	void setBuffValue(float newValue);

	UFUNCTION(BlueprintCallable)
	void setBuffInterval(float newInterval);

	UFUNCTION(BlueprintCallable)
	void setBuffTicks(int newTicks);
	
};
