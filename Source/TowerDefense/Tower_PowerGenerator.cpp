// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower_PowerGenerator.h"

#include "Buff_Overload.h"
#include "Components/SphereComponent.h"

ATower_PowerGenerator::ATower_PowerGenerator()
{
}

void ATower_PowerGenerator::BeginPlay()
{
	ATowerBase::BeginPlay();
	const FTimerDelegate delegate = FTimerDelegate::CreateUObject
					(this, &ATower_PowerGenerator::TowerBehavior);

	GetWorld()->GetTimerManager().SetTimer(BehaviorTimer, delegate,BehaviorInterval,true,0.1f);
}

void ATower_PowerGenerator::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	
}

void ATower_PowerGenerator::UpgradeStats()
{
	int i = Level-1;
	BehaviorValue = DataAsset->LevelProperties[i].Value;
	BehaviorInterval = DataAsset->LevelProperties[i].Interval;
	ZoneSphere->SetSphereRadius(DataAsset->LevelProperties[i].RadiusRange);

	Description.Empty();
	Description.Append("-"+FString::SanitizeFloat(BehaviorValue*100)+"% attack speed to nearby towers" + LINE_TERMINATOR);
	Description.Append("Tower Radius: " + FString::SanitizeFloat(ZoneSphere->GetScaledSphereRadius())+ " Units");
}

void ATower_PowerGenerator::TowerBehavior()
{
	TArray<AActor*> TargetsInRange;
	ZoneSphere->GetOverlappingActors(TargetsInRange);

	for(AActor* actor : TargetsInRange)
	{
		if(ATowerBase* tower = Cast<ATowerBase>(actor))
		{
			ApplyAndRefreshBuff(tower, UBuff_Overload::StaticClass());
		}
	}
}