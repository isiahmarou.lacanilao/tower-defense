// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TowerBase.h"
#include "Tower_Salvager.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATower_Salvager : public ATowerBase
{
	GENERATED_BODY()
public:
	ATower_Salvager();
	
protected:
	virtual void BeginPlay() override;
	
	virtual void TowerBehavior() override;

	void MarkNearbyEnemies();
	
	UFUNCTION()
	void MarkedEnemyDeath();

	class APlayerPawn* Player;

	virtual void UpgradeStats() override;
	
};
