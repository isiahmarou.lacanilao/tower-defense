// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuffComponent.h"
#include "EnemyBase.h"

#include "Buff_Nanobot.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEnemyKill, class AEnemyBase*, enemy);
UCLASS()
class TOWERDEFENSE_API UBuff_Nanobot : public UBuffComponent
{
	GENERATED_BODY()
public:
    UBuff_Nanobot();

protected:
    virtual void BeginPlay() override;
    
    virtual void BuffBehavior() override;

	AEnemyBase* enemy;
public:
	FOnEnemyKill OnEnemyKill;
};
