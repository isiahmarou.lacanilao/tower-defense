// Fill out your copyright notice in the Description page of Project Settings.


#include "Buff_Slow.h"

#include "EnemyBase.h"

UBuff_Slow::UBuff_Slow()
{
	Value = 0.5;
	Interval =1.f;
	BuffTicks = 3;
}

void UBuff_Slow::BeginPlay()
{
	Super::BeginPlay();
	enemy = Cast<AEnemyBase>(owner);
	ownerSpeed = enemy->getMoveSpeed();

	UE_LOG(LogTemp, Warning, TEXT("Slow buff active"));


}

void UBuff_Slow::BuffBehavior()
{
	const float temp = ownerSpeed*Value;
	enemy->setMoveSpeed(ownerSpeed - temp);
	
	currentTick--;
	if(currentTick<=0)
	{
		GetWorld()->GetTimerManager().ClearTimer(BuffTimerHandle);
		
		enemy->setMoveSpeed(ownerSpeed);
		
		DestroyComponent();
	}
}

void UBuff_Slow::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	enemy->setMoveSpeed(ownerSpeed);
}
