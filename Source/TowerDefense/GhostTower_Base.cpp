#include "GhostTower_Base.h"
#include "BuildablePlatform_Base.h"
#include "Materials/Material.h"
#include "Components/SphereComponent.h"

AGhostTower_Base::AGhostTower_Base()
{
	PrimaryActorTick.bCanEverTick = false;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootScene"));
	Range = CreateDefaultSubobject<USphereComponent>(TEXT("Range"));
	Range->SetupAttachment(RootComponent);
	Range->SetHiddenInGame(false);
	Range->SetSphereRadius(850.f);
}

void AGhostTower_Base::BeginPlay()
{
	Super::BeginPlay();

	for(int i =0;i<TowerMeshes.Num();i++)
	{
		TowerMeshes[i]->SetCollisionProfileName("IgnoreTrace");
	}
	TowerMeshes[0]->OnComponentBeginOverlap.AddDynamic(this, &AGhostTower_Base::OnOverlapEnter);
	TowerMeshes[0]->OnComponentEndOverlap.AddDynamic(this, &AGhostTower_Base::OnOverlapExit);
}

void AGhostTower_Base::ChangeMeshMaterial(class UMaterial* newMaterial)
{
	for (int i=0; i<TowerMeshes.Num() ;i++)
	{
		TowerMeshes[i]->SetMaterial(0, newMaterial);
	}
}

void AGhostTower_Base::OnOverlapEnter(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(ABuildablePlatform_Base* platform = Cast<ABuildablePlatform_Base>(OtherActor))
	{
		if(platform->IsBuildable())
		{
			ChangeMeshMaterial(GreenMaterial);
		}
	}
}

void AGhostTower_Base::OnOverlapExit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if(ABuildablePlatform_Base* platform = Cast<ABuildablePlatform_Base>(OtherActor))
	{
		ChangeMeshMaterial(RedMaterial);
	}
}




