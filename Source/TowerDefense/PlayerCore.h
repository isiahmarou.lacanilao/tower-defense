// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlayerCore.generated.h"

UCLASS()
class TOWERDEFENSE_API APlayerCore : public AActor
{
	GENERATED_BODY()
	
public:	
	APlayerCore();

protected:
	virtual void BeginPlay() override;

	UPROPERTY()
	class APlayerPawn* Player;

public:	
	virtual void Tick(float DeltaTime) override;

	void damagePlayerHealth(float damage, APawn* DamageCauser);
};
