// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Waypoint.h"

#include "EnemyController.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AEnemyController : public AAIController
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;

	void MoveToWaypoint();

	class AEnemyBase* owner;
	
	TArray<AWaypoint*> Waypoints;

	int currentWaypoint;
public:
	void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;

	UFUNCTION(BlueprintCallable)
	void GoToCore(TArray<AWaypoint*> LanePath);

};
