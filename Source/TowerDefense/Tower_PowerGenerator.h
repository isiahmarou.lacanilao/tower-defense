// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TowerBase.h"
#include "Tower_PowerGenerator.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATower_PowerGenerator : public ATowerBase
{
	GENERATED_BODY()

public:
	ATower_PowerGenerator();
	
protected:
	virtual void BeginPlay() override;
		
	virtual void UpgradeStats() override;

	virtual void TowerBehavior() override;
	
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
};
