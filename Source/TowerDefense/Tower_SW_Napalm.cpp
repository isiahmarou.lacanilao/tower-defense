// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower_SW_Napalm.h"

#include "EnemyBase.h"
#include "TowerDefenseController.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Widget_SkillUI.h"

ATower_SW_Napalm::ATower_SW_Napalm()
{
	DamageAreaScene = CreateDefaultSubobject<USceneComponent>(TEXT("DamageAreaScene"));
	
	DamageAreaMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DamageAreaMesh"));
	DamageAreaMesh->SetupAttachment(DamageAreaScene);
	DamageAreaMesh->SetRelativeScale3D(FVector(30.f,30.,30.f));
	this->AddInstanceComponent(DamageAreaMesh);
		
	DamageAreaMesh->SetActive(false);
	DamageAreaMesh->SetVisibility(false);
}

void ATower_SW_Napalm::BeginPlay()
{
	ATower_SuperWeaponBase::BeginPlay();
}

void ATower_SW_Napalm::OnSkillActivate()
{
	if(IsOnCoolDown())
	{
		ATowerDefenseController* _Controller = Cast<ATowerDefenseController>(UGameplayStatics::GetPlayerController(GetWorld(),0));
		_Controller->InputComponent->BindAction("RightMouseClick", IE_Pressed,this, &ATower_SW_Napalm::OnRightClick);

		DamageAreaMesh->SetActive(true);
		DamageAreaMesh->SetVisibility(true);

		 const FTimerDelegate delegate = FTimerDelegate::CreateUObject
		 			(this, &ATower_SW_Napalm::SelectArea, _Controller);
		
		 FTimerHandle followMouse;
		 GetWorld()->GetTimerManager().SetTimer(followMouse, delegate,.1,true,0);
	}
}

void ATower_SW_Napalm::OnRightClick()
{
	if(DamageAreaMesh->IsVisible())
	{
		TArray<AActor*> overlapActors;
		DamageAreaMesh->GetOverlappingActors(overlapActors,AEnemyBase::StaticClass());
	
		for(AActor* actor : overlapActors)
		{
			if(AEnemyBase* enemy = Cast<AEnemyBase>(actor))
			{
				enemy->TakeDamage(BehaviorValue,FDamageEvent(),nullptr,nullptr);
			}
		}
	
		DamageAreaMesh->SetVisibility(false);
		
		ATowerDefenseController* _Controller = Cast<ATowerDefenseController>(UGameplayStatics::GetPlayerController(GetWorld(),0));
		_Controller->InputComponent->RemoveActionBinding("RightMouseClick", IE_Pressed);

		// Start Cooldown
		this->CoolDownStarts.Broadcast(this->GetClass());
		CountDown = BehaviorInterval;
		
		const FTimerDelegate delegate = FTimerDelegate::CreateUObject
					(this, &ATower_SW_Napalm::OnCountDown);
		
		GetWorld()->GetTimerManager().SetTimer(BehaviorTimer, delegate,1.f,true);
	}
}

void ATower_SW_Napalm::SelectArea(ATowerDefenseController* pc)
{
	if(DamageAreaMesh->IsVisible())
	{
		FHitResult OutHit;
		pc->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery3, true, OutHit);

		DamageAreaScene->SetWorldLocation(OutHit.Location);
	}
}

void ATower_SW_Napalm::UpgradeStats()
{
	int i = Level-1;
	BehaviorInterval = DataAsset->LevelProperties[i].Interval;
	BehaviorValue = DataAsset->LevelProperties[i].Value;
	
	float scale = DataAsset->LevelProperties[i].RadiusRange;
	DamageAreaMesh->SetRelativeScale3D(FVector(scale,scale,scale));
	
	FVector relativeLoc = DamageAreaMesh->GetRelativeLocation();
	relativeLoc.Z = scale* -50.f;
	DamageAreaMesh->SetRelativeLocation(relativeLoc);
	
	Description.Empty();
	Description.Append("Grants the Napalm Skill" LINE_TERMINATOR);
	Description.Append("Damage : " + FString::SanitizeFloat(BehaviorValue) + LINE_TERMINATOR);
	Description.Append("Attack Scale: " + FString::SanitizeFloat(scale));
}

void ATower_SW_Napalm::UpgradeMesh()
{
	ATowerBase::UpgradeMesh();
	BaseMesh->SetRelativeScale3D(FVector(5.f,5.f,5.f));
}


