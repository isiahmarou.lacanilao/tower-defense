// Copyright Epic Games, Inc. All Rights Reserved.


#include "TowerDefenseGameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "EnemyBase.h"
#include "PlayerPawn.h"
#include "WaveDataAsset.h"
#include "SpawnerEnemy.h"
#include "HealthComponent.h"
#include "Blueprint/WidgetLayoutLibrary.h"

void ATowerDefenseGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	WaveNum = 0;
	UWorld* World = GetWorld();

	if (World)
	{
		mPlayerPawn = Cast<APlayerPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));

		UAudioComponent* bgm = UGameplayStatics::SpawnSound2D(this, BackgroundMusic, 1.f);

		WaveEnds.Broadcast(this);
	}
}

void ATowerDefenseGameModeBase::SpawnWave(UWaveDataAsset* WaveDA)
{
	//UE_LOG(LogTemp, Warning, TEXT("Wave num: %i"), WaveNum);
	
	// check if last wave
	if(WaveNum == WavesDA_Arr.Num())
	{
		//UE_LOG(LogTemp, Warning, TEXT("Last Wave"));
		
		AEnemyBase* boss_1 = Spawners_Arr[0]->InitEnemy(WaveDA->BossClass);
		AEnemyBase* boss_2 = Spawners_Arr[2]->InitEnemy(WaveDA->BossClass);

		boss_1->getEnemyHealthComponent()->scaleMaxHealth(WaveDA->enemyHealthScale);
		boss_1->OnEnemyDies.AddDynamic(this, &ATowerDefenseGameModeBase::OnEnemyDeath);

		boss_2->getEnemyHealthComponent()->scaleMaxHealth(WaveDA->enemyHealthScale);
		boss_2->OnEnemyDies.AddDynamic(this, &ATowerDefenseGameModeBase::OnEnemyDeath);

		Enemies_Arr.Push(boss_1);
		Enemies_Arr.Push(boss_2);
		currentNumToSpawn =0;
		GetWorldTimerManager().ClearTimer(SpawnEnemyTimerHandle);
		return;
	}

	int s = FMath::RandRange(0, Spawners_Arr.Num() - 1);
	
	// spawn random regular enemy
	if(currentNumToSpawn > 1)
	{
		// Find Random EnemyClass 
		float TotalWeight=0;
		
		for (int i = 0; i < WaveDA->EnemySpecs.Num(); i++)
		{
			TotalWeight += WaveDA->EnemySpecs[i].EnemySpawnChance;
		}

		float rng = FMath::FRandRange(0.f, TotalWeight);

		for (int i = 0; i < WaveDA->EnemySpecs.Num(); i++)
		{
			if (rng <= WaveDA->EnemySpecs[i].EnemySpawnChance)
			{
				//Spawn Enemy
				AEnemyBase* enemy = Spawners_Arr[s]->InitEnemy(WaveDA->EnemySpecs[i].EnemyClass);
				enemy->getEnemyHealthComponent()->scaleMaxHealth(WaveDA->enemyHealthScale); /////
				enemy->OnEnemyDies.AddDynamic(this, &ATowerDefenseGameModeBase::OnEnemyDeath);

				Enemies_Arr.Push(enemy);
				currentNumToSpawn--;
				return;
			}
			rng -= WaveDA->EnemySpecs[i].EnemySpawnChance;
		}
	}
	// spawn boss
	else if(currentNumToSpawn ==1)
	{
		AEnemyBase* boss = Spawners_Arr[s]->InitEnemy(WaveDA->BossClass);
		boss->getEnemyHealthComponent()->scaleMaxHealth(WaveDA->enemyHealthScale);
		boss->OnEnemyDies.AddDynamic(this, &ATowerDefenseGameModeBase::OnEnemyDeath);

		Enemies_Arr.Push(boss);
		currentNumToSpawn--;
	}

	// Wave Ends
	else
	{
		isWaveInProgress= false;
	}
}

void ATowerDefenseGameModeBase::OnTowerDefense()
{
	if(isWaveInProgress) return;
	
	if(WaveNum < WavesDA_Arr.Num())
	{
		currentWaveDA = WavesDA_Arr[WaveNum];
		WaveNum++;
		TotalNumToSpawn = currentWaveDA->TotalNumSpawns;
		currentNumToSpawn = TotalNumToSpawn;
		SpawnInterval = currentWaveDA->SpawnInterval;
		isWaveInProgress = true;

		FTimerDelegate EnemyWaves_Delegate = FTimerDelegate::CreateUObject(this, &ATowerDefenseGameModeBase::SpawnWave, currentWaveDA);
		GetWorld()->GetTimerManager().SetTimer(SpawnEnemyTimerHandle, EnemyWaves_Delegate, SpawnInterval, true);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("This Should not be called but whatever"));
		OnGameEnds(true);
	}
}

void ATowerDefenseGameModeBase::OnEnemyDeath(class AEnemyBase* enemy, bool isKilledByPlayer)
{
	if (isKilledByPlayer)
	{
		KillCount++;
		
		int32 gold  = enemy->getGoldDrop() * currentWaveDA->enemyGoldDropScale;
		mPlayerPawn->addGoldAmount(gold);
	}
	Enemies_Arr.Remove(enemy);

	//wave ends
	if(Enemies_Arr.Num()<=0 && currentNumToSpawn<=0)
	{
		//UE_LOG(LogTemp, Warning, TEXT("No more Enemies"));
		
		GetWorldTimerManager().ClearTimer(SpawnEnemyTimerHandle);
		isWaveInProgress = false;
		mPlayerPawn->addGoldAmount(currentWaveDA->ClearReward);
		if(WaveNum >= WavesDA_Arr.Num())
		{
			//UE_LOG(LogTemp, Warning, TEXT("Game Should End"));
			OnGameEnds(true);
		}
		else
		{
			WaveEnds.Broadcast(this);
			//OnTowerDefense();
		}
	}

}


