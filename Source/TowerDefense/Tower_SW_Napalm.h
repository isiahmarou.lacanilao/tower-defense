// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower_SuperWeaponBase.h"
#include "Tower_SW_Napalm.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATower_SW_Napalm : public ATower_SuperWeaponBase
{
	GENERATED_BODY()
public:
	ATower_SW_Napalm();
protected:
	virtual void BeginPlay() override;
	
	UFUNCTION()
	void SelectArea(class ATowerDefenseController* pc);

	virtual void UpgradeStats() override;

	virtual void UpgradeMesh() override;
	UFUNCTION()
	void OnRightClick();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USceneComponent* DamageAreaScene;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UStaticMeshComponent* DamageAreaMesh;


public:
	virtual void OnSkillActivate() override;

};
