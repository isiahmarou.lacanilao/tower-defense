// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnerEnemy.h"
#include "EnemyBase.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"
#include "EnemyController.h"

ASpawnerEnemy::ASpawnerEnemy()
{
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	RootComponent = SceneComponent;

	SpawnArea = CreateDefaultSubobject<UBoxComponent>("SpawnArea");
	SpawnArea->SetupAttachment(SceneComponent);
}

void ASpawnerEnemy::BeginPlay()
{
	Super::BeginPlay();
}

void ASpawnerEnemy::giveLaneWaypointsToEnemy(class AEnemyController* eController)
{
	switch (mLane)
	{
	case LaneToFollow::LEFT:
		if (LeftLane_Waypoints.Num() > 0) eController->GoToCore(LeftLane_Waypoints);
		break;

	case LaneToFollow::RIGHT:
		if (RightLane_Waypoints.Num() > 0) eController->GoToCore(RightLane_Waypoints);
		break;

	case LaneToFollow::MIDDLE:
		float j = FMath::FRandRange(0.0f, 1.f);
		if (j >= 0.5f)
		{
			if (LeftLane_Waypoints.Num() > 0) eController->GoToCore(LeftLane_Waypoints);
		}
		else
		{
			if (RightLane_Waypoints.Num() > 0) eController->GoToCore(RightLane_Waypoints);
		}
		break;
	}
} ////

AEnemyBase* ASpawnerEnemy::InitEnemy(TSubclassOf<class AEnemyBase> EnemyClass)
{
	if (UWorld* World = GetWorld())
	{
		if (EnemyClass)
		{
			FActorSpawnParameters SpawnParameters;
			SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

			const FVector fLocation = this->SpawnArea->GetComponentLocation();

			AEnemyBase* enemyObject = World->SpawnActor<AEnemyBase>(EnemyClass, fLocation, FRotator::ZeroRotator, SpawnParameters);
			
			if (AEnemyController* eController = Cast<AEnemyController>(enemyObject->GetController()))
			{
				giveLaneWaypointsToEnemy(eController);
			}
			return enemyObject; ////
		}
	}
	return nullptr;
}
