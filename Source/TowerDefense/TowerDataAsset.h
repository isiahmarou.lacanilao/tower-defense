// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "TowerDataAsset.generated.h"

USTRUCT(BlueprintType)
struct FLevelProperty
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 Cost;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Value;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Interval;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float RadiusRange;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMesh* BaseMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMesh* GunMesh;
};
UCLASS(BlueprintType)
class TOWERDEFENSE_API UTowerDataAsset : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere,BlueprintReadOnly)
	FString TowerName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class ATowerBase> TowerClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class AGhostTower_Base> GhostTowerClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FLevelProperty> LevelProperties;
};
