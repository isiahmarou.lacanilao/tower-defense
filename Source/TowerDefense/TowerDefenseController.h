// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TowerDefenseController.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATowerDefenseController : public APlayerController
{
	GENERATED_BODY()
	
public:
	ATowerDefenseController();
protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class APlayerPawn* playerPawn;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USpringArmComponent* SpringArmComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UCameraComponent* CameraComponent;

	UPROPERTY(EditAnywhere)
	float ScreenMargin;

	UPROPERTY(EditAnywhere)
	float Speed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 ScreenSizeX;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 ScreenSizeY;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float ZoomScale;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float ArmLengthMin;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float ArmLengthMax;

	UPROPERTY()
	class ABuildManager* BuildManager;
	
	void MoveForward(float scale);
	void MoveRight(float scale);

	void ZoomIn();
	void ZoomOut();

	UFUNCTION()
	void OnLeftClick();
public:
	virtual void Tick(float DeltaTime) override;

	virtual void SetupInputComponent() override;

	FVector getCameraPanDirection();

	FORCEINLINE void setBuildManager(ABuildManager* bm) { BuildManager = bm;}
	
	UFUNCTION()
	void GhostFollowMouse(class AGhostTower_Base* towerGhost);
};