 // Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawn.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "HealthComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "TowerDefenseController.h"
#include "TowerDefenseGameModeBase.h"
#include "Kismet/GameplayStatics.h"

 APlayerPawn::APlayerPawn()
{
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	RootComponent = SceneComponent;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
	SpringArm->bDoCollisionTest = false;

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");

	SpringArm->SetupAttachment(SceneComponent);
	Camera->SetupAttachment(SpringArm);

	PlayerMovement = CreateDefaultSubobject<UFloatingPawnMovement>("PlayerMovement");
	playerHealthComponent = HealthComponent;

}

void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	HealthComponent->OnPawnDeath.AddDynamic(this, &APlayerPawn::OnDeath);

	mController = Cast<ATowerDefenseController>(this->GetController());
}


void APlayerPawn::OnDeath()
{
	ABase_CharacterPawn::OnDeath();

	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Player Dies")));
	}

	ATowerDefenseGameModeBase* GameMode = Cast<ATowerDefenseGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
 	GameMode->OnGameEnds(false);
}

void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APlayerPawn::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}


void APlayerPawn::addGoldAmount(int32 additionalGold)
{
	GoldAmount += additionalGold;
	//if (GEngine)
	//{
	//	//Print debug message
	//	GEngine->AddOnScreenDebugMessage(-10, 3.f, FColor::Yellow, FString::Printf(TEXT("NewGold - AddedGold: %i - %i"), GoldAmount, additionalGold));
	//}
}

void APlayerPawn::subtractGoldAmount(int32 value)
{
	GoldAmount -= value;
}

int32 APlayerPawn::getGoldAmount()
{
	return GoldAmount;
}
