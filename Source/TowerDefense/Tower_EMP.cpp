// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower_EMP.h"
#include "Buff_Slow.h"
#include "EnemyBase.h"
#include "Components/SphereComponent.h"

ATower_EMP::ATower_EMP()
{
	//ATowerBase();
}

void ATower_EMP::BeginPlay()
{
	ATowerBase::BeginPlay();
	//ZoneSphere->OnComponentBeginOverlap.AddDynamic(this,&ATower_EMP::OnZoneEnter);
	//ZoneSphere->OnComponentEndOverlap.AddDynamic(this,&ATower_EMP::OnZoneExit);

	const FTimerDelegate delegate = FTimerDelegate::CreateUObject
					(this, &ATower_EMP::TowerBehavior);

	GetWorld()->GetTimerManager().SetTimer(BehaviorTimer, delegate,1.f,true,0.1f);
}

void ATower_EMP::TowerBehavior()
{
	TArray<AEnemyBase*> Enemy_Arr;
	getNearbyEnemies(Enemy_Arr);

	for(AEnemyBase* enemy : Enemy_Arr)
	{
		ApplyAndRefreshBuff(enemy,UBuff_Slow::StaticClass());
	}
}

void ATower_EMP::UpgradeStats()
{
	int i = Level-1;
	BehaviorValue = DataAsset->LevelProperties[i].Value;
	ZoneSphere->SetSphereRadius(DataAsset->LevelProperties[i].RadiusRange);

	Description.Empty();
	Description.Append("-"+FString::SanitizeFloat(BehaviorValue*100)+"% speed to nearby enemies" + LINE_TERMINATOR);
	Description.Append("Tower Radius: " + FString::SanitizeFloat(ZoneSphere->GetScaledSphereRadius())+ " Units");
}

