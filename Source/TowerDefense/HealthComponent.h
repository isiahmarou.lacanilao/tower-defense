// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPawnDeath);
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOWERDEFENSE_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UHealthComponent();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	float maxHealth;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float currentHealth;

	UFUNCTION()
	void TakeDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

public:
	UFUNCTION(BlueprintPure, BlueprintCallable)
	float getHealthPercentage();

	UFUNCTION(BlueprintPure, BlueprintCallable)
	float getCurrentHealth();

	UFUNCTION(BlueprintPure, BlueprintCallable)
	float getMaxHealth();

	UFUNCTION(BlueprintCallable)
	void scaleMaxHealth(float valueScale);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FPawnDeath OnPawnDeath;
};

