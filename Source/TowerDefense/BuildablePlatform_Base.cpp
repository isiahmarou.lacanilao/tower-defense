// Fill out your copyright notice in the Description page of Project Settings.


#include "BuildablePlatform_Base.h"
#include "Components/BoxComponent.h"
#include "BuildManager.h"
#include "TowerBase.h"

ABuildablePlatform_Base::ABuildablePlatform_Base()
{

	PrimaryActorTick.bCanEverTick = false;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	RootComponent = SceneComponent;

	PlatformMesh = CreateDefaultSubobject<UStaticMeshComponent>("PlatformMesh");
	PlatformMesh->SetupAttachment(RootComponent);

	BoxCollision = CreateDefaultSubobject<UBoxComponent>("BoxCollision");
	BoxCollision->SetupAttachment(PlatformMesh);
}

void ABuildablePlatform_Base::BeginPlay()
{
	Super::BeginPlay();
}

bool ABuildablePlatform_Base::IsBuildable()
{
	if(!IsValid(TowerOnPlatform))
	{
		return true;
	}
	return false;
}
