// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerDefenseController.h"
#include "BuildablePlatform_Base.h"
#include "BuildManager.h"
#include "PlayerPawn.h"
#include "GameFramework/SpringArmComponent.h"
#include "GhostTower_Base.h"
#include "TowerBase.h"


ATowerDefenseController::ATowerDefenseController()
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableMouseOverEvents = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;

	ScreenMargin = 10.f;
	Speed = 20.f;
	ZoomScale = 100;
	ArmLengthMin = 1000.f;
	ArmLengthMax = 4000.f;
}

void ATowerDefenseController::BeginPlay()
{
	Super::BeginPlay();

	playerPawn = Cast<APlayerPawn>(this->GetPawn());

	SpringArmComponent = playerPawn->getSpringArm();

	CameraComponent = playerPawn->getCamera();

	GetViewportSize(ScreenSizeX, ScreenSizeY);
}

void ATowerDefenseController::MoveForward(float scale)
{
	playerPawn->AddMovementInput(playerPawn->GetActorForwardVector(), scale);
}

void ATowerDefenseController::MoveRight(float scale)
{
	playerPawn->AddMovementInput(playerPawn->GetActorRightVector(), scale);
}

void ATowerDefenseController::ZoomIn()
{
	//SpringArmComponent->TargetArmLength -= ZoomScale;

	SpringArmComponent->TargetArmLength = FMath::Clamp(SpringArmComponent->TargetArmLength -= ZoomScale, ArmLengthMin, ArmLengthMax);
}

void ATowerDefenseController::ZoomOut()
{
	//SpringArmComponent->TargetArmLength += ZoomScale;

	SpringArmComponent->TargetArmLength = FMath::Clamp(SpringArmComponent->TargetArmLength += ZoomScale, ArmLengthMin, ArmLengthMax);
}

void ATowerDefenseController::OnLeftClick()
{
	FHitResult hit;
	GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery2, true, hit);

	FString name = hit.Actor->GetName();

	if(BuildManager->getIsInBuildMode())
	{
		if(ABuildablePlatform_Base* platform = Cast<ABuildablePlatform_Base>(hit.Actor))
		{
			if(platform->IsBuildable())
			{
				BuildManager->BuildTowerOnPlatform(platform);
				UE_LOG(LogTemp, Warning, TEXT("true"));
			}
			else UE_LOG(LogTemp, Warning, TEXT("false"));
		}
	}
	else
	{
		if(ATowerBase* tower = Cast<ATowerBase>(hit.Actor))
		{
			BuildManager->ShowTowerWidget(tower);
		}
	}
}

void ATowerDefenseController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//if (Player)
	//{
	//	playerPawn->AddActorWorldOffset(getCameraPanDirection() * Speed);
	//}
}

void ATowerDefenseController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveRight", this, &ATowerDefenseController::MoveRight);
	InputComponent->BindAxis("MoveForward", this, &ATowerDefenseController::MoveForward);

	InputComponent->BindAction("ZoomIn", IE_Pressed, this, &ATowerDefenseController::ZoomIn);
	InputComponent->BindAction("ZoomOut", IE_Pressed, this, &ATowerDefenseController::ZoomOut);

	InputComponent->BindAction("LeftMouseClick", IE_Pressed, this, &ATowerDefenseController::OnLeftClick);
}

FVector ATowerDefenseController::getCameraPanDirection()
{
	float MousePosX;
	float MousePosY;

	float CamDirectionX = 0;
	float CamDirectionY = 0;

	this->GetMousePosition(MousePosX, MousePosY);

	if (MousePosX <= ScreenMargin)
	{
		CamDirectionY = -1;
	}
	if (MousePosY <= ScreenMargin)
	{
		CamDirectionX = 1;
	}
	if (MousePosX >= ScreenSizeX - ScreenMargin)
	{
		CamDirectionY = 1;
	}
	if (MousePosY >= ScreenSizeY - ScreenMargin)
	{
		CamDirectionX = -1;
	}

	return FVector(CamDirectionX, CamDirectionY, 0);
}


void ATowerDefenseController::GhostFollowMouse(AGhostTower_Base* towerGhost)
{
	if(towerGhost)
	{
		FHitResult OutHit;
		this->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery1, true, OutHit);

		if (ABuildablePlatform_Base* platform = Cast<ABuildablePlatform_Base>(OutHit.GetActor()))
		{
			if(platform->IsBuildable())
			{
				towerGhost->SetActorLocationAndRotation(platform->GetActorLocation(), platform->getMeshComponentRotation());
				return;
			}
		}
		towerGhost->SetActorLocation(OutHit.Location);
	}
}


