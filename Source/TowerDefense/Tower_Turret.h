// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "EnemyBase.h"
#include "TowerBase.h"
#include "Tower_Turret.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATower_Turret : public ATowerBase
{
	GENERATED_BODY()
public:
	ATower_Turret();
protected:
	virtual void BeginPlay() override;

	virtual void TowerBehavior() override;

	UPROPERTY(EditAnywhere,BlueprintReadOnly)
	UStaticMeshComponent* GunMesh_;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="ATOWERBASE")
	class USceneComponent* Pivot_;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="ATOWERBASE")
	class UArrowComponent* Muzzle_;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="ATowerTurret")
	TSubclassOf<class AProjectileBase> BulletClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="ATowerTurret")
	class USoundBase* turretSFX;

	AEnemyBase* currentTarget;
	
	virtual void UpgradeStats() override;

	virtual void UpgradeMesh() override;


};
