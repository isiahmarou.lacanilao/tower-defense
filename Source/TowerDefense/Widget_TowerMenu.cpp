// Fill out your copyright notice in the Description page of Project Settings.


#include "Widget_TowerMenu.h"
#include "Components/Button.h"
#include "TowerBase.h"
#include "BuildManager.h"
#include "PlayerPawn.h"
#include "Components/TextBlock.h"

bool UWidget_TowerMenu::Initialize()
{
	Super::Initialize();
	
	this->OnVisibilityChanged.AddDynamic(this, &UWidget_TowerMenu::OnVisibilityChange);
	
	Upgrade_BTN->OnClicked.AddDynamic(this, &UWidget_TowerMenu::OnUpgradeBTNClicked);
	
	Sell_BTN->OnClicked.AddDynamic(this, &UWidget_TowerMenu::OnSellBTNClicked);

	// this->OnMouseLeave()
	return true;
}

void UWidget_TowerMenu::OnVisibilityChange(ESlateVisibility InVisibility)
{
	switch(InVisibility)
	{
		case ESlateVisibility::Visible:
			if(IsValid(Tower))
			{
				Upgrade_BTN->SetIsEnabled(true);
				ChangeInfos();
			}
			else
			{
				this->SetVisibility(ESlateVisibility::Hidden);
			}
			break;
		case ESlateVisibility::Hidden:
			Tower = nullptr;
			break;
		default:
			break;
	}
}

void UWidget_TowerMenu::ChangeInfos()
{
	if(!Tower)
	{
		UE_LOG(LogTemp, Warning, TEXT("Somethingis very wrong") );
		this->SetVisibility(ESlateVisibility::Hidden);
		return;
	}
	Name_TXT->SetText(FText::FromString(Tower->getDataAsset()->TowerName));

	// FString LevelString;
	// LevelString.Append("Level " + FString::SanitizeFloat(Tower->getLevel()));
	Level_TXT->SetText(FText::FromString("Level " + FString::FromInt(Tower->getLevel())));

	Description_TXT->SetText(FText::FromString(Tower->getDescription()));

	if(Tower->IsMaxLevel())
	{
		Upgrade_BTN->SetIsEnabled(false);
		Upgrade_TXT->SetText(FText::FromString("Maxed Upgrade"));
	}
	else
	{
		
		int index = Tower->getLevel();
		int cost = Tower->getDataAsset()->LevelProperties[index].Cost;
		Upgrade_TXT->SetText(FText::FromString("Upgrade for $" + FString::FromInt(cost)));
		
		if(cost > player->getGoldAmount())
		{
			Upgrade_BTN->SetIsEnabled(false);
		}
	}

	Sell_TXT->SetText(FText::FromString("Sell for $" + FString::FromInt(Tower->getSellValue())));
}

void UWidget_TowerMenu::OnUpgradeBTNClicked()
{
	if(!Tower)
	{
		UE_LOG(LogTemp, Warning, TEXT("Somethingis very wrong") );
		return;
	}
	
	if(!Tower->IsMaxLevel())
	{
		int index = Tower->getLevel();
		int cost = Tower->getDataAsset()->LevelProperties[index].Cost;

		if(cost <= player->getGoldAmount())
		{
			player->subtractGoldAmount(cost);
			Tower->OnUpgrade();
			ChangeInfos();
			return;
		}
	}

	//if tower is max level or player cant afford upgrade, disable the button
	Upgrade_BTN->SetIsEnabled(false);
}

void UWidget_TowerMenu::OnSellBTNClicked()
{
	BuildManager->SellTower(Tower);
	SetVisibility(ESlateVisibility::Hidden);
}
