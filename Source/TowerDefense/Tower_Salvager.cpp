// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower_Salvager.h"

#include "Buff_Marked.h"
#include "EnemyBase.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerPawn.h"
#include "Components/SphereComponent.h"

ATower_Salvager::ATower_Salvager()
{
	
}

void ATower_Salvager::BeginPlay()
{
	ATowerBase::BeginPlay();
	Player = Cast<APlayerPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(),0));


	const FTimerDelegate Delegate = FTimerDelegate::CreateUObject(this, &ATower_Salvager::TowerBehavior);
	GetWorld()->GetTimerManager().SetTimer(BehaviorTimer, Delegate, BehaviorInterval, true);

	const FTimerDelegate markingDelegate = FTimerDelegate::CreateUObject(this, &ATower_Salvager::MarkNearbyEnemies);
	FTimerHandle markingHandle;
	GetWorld()->GetTimerManager().SetTimer(markingHandle, markingDelegate, 1.f, true);
}

void ATower_Salvager::TowerBehavior()
{
	Player->addGoldAmount(BehaviorValue);
}

void ATower_Salvager::MarkNearbyEnemies()
{
	TArray<AEnemyBase*> Enemy_Arr;
	getNearbyEnemies(Enemy_Arr);

	for(AEnemyBase* enemy : Enemy_Arr)
	{
		if(UBuff_Marked* markedComponent = Cast<UBuff_Marked>(ApplyAndRefreshBuff(enemy,UBuff_Marked::StaticClass())))
		{
			markedComponent->OwnerKilled.AddDynamic(this, &ATower_Salvager::MarkedEnemyDeath);
		}
	}
}

void ATower_Salvager::MarkedEnemyDeath()
{
	UE_LOG(LogTemp, Warning, TEXT("Death: %f"),GetLastRenderTime() );

	Player->addGoldAmount(BehaviorValue);
}

void ATower_Salvager::UpgradeStats()
{
	int i = Level-1;
	BehaviorInterval = DataAsset->LevelProperties[i].Interval;
	BehaviorValue = DataAsset->LevelProperties[i].Value;
	ZoneSphere->SetSphereRadius(DataAsset->LevelProperties[i].RadiusRange);
	
	Description.Empty();
	Description.Append("Passive Gold: " + FString::SanitizeFloat(BehaviorValue) + LINE_TERMINATOR);
	Description.Append("per "+FString::SanitizeFloat(BehaviorInterval) + " second" + LINE_TERMINATOR);
	Description.Append("Tower Radius: " + FString::SanitizeFloat(ZoneSphere->GetScaledSphereRadius())+ " Units");
}

