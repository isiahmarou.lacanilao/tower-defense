// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TowerBase.h"
#include "Tower_LaserTurret.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATower_LaserTurret : public ATowerBase
{
	GENERATED_BODY()
public:
	ATower_LaserTurret();
protected:
	virtual void BeginPlay() override;

	virtual void TowerBehavior() override;

	UPROPERTY(EditAnywhere,BlueprintReadOnly)
	UStaticMeshComponent* GunMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="ATOWERBASE")
	class USceneComponent* Pivot;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="ATOWERBASE")
	class UArrowComponent* Muzzle;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="ATower_LaserTurret")
	TSubclassOf<class AProjectileBase> LaserClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="ATower_LaserTurret")
	UParticleSystem* BurnGFX;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="ATower_LaserTurret")
	class USoundBase* laserSFX;
	
	AEnemyBase* currentTarget;
	
	virtual void UpgradeStats() override;
	virtual void UpgradeMesh() override;
};
