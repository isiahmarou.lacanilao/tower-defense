// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuffComponent.h"
#include "Buff_Slow.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API UBuff_Slow : public UBuffComponent
{
	GENERATED_BODY()
public:
	UBuff_Slow();
protected:
	virtual void BeginPlay() override;

	virtual void BuffBehavior() override;

	class AEnemyBase* enemy;
	
	float ownerSpeed;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

};
