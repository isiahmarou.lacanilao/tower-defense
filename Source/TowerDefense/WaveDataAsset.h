// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "WaveDataAsset.generated.h"

USTRUCT(BlueprintType)
struct FEnemySpecs
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString EnemyName;

	UPROPERTY(EditAnywhere,BlueprintReadOnly)
	TSubclassOf<class AEnemyBase> EnemyClass;

	UPROPERTY(EditAnywhere,BlueprintReadOnly)
	float EnemySpawnChance;
};

UCLASS(BlueprintType)
class TOWERDEFENSE_API UWaveDataAsset : public UDataAsset
{
	GENERATED_BODY()
protected:
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float SpawnInterval;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32  TotalNumSpawns;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FEnemySpecs> EnemySpecs;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float enemyHealthScale;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float enemyGoldDropScale;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class AEnemyBase> BossClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly,Category = "Reward")
	int32 ClearReward;

};
