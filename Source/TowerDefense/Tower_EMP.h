// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TowerBase.h"
#include "Tower_EMP.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATower_EMP : public ATowerBase
{
	GENERATED_BODY()
public:
	ATower_EMP();
protected:
	virtual void BeginPlay() override;

	virtual void TowerBehavior() override;



	virtual void UpgradeStats() override;
};
