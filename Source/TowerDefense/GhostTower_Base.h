// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GhostTower_Base.generated.h"

UCLASS()
class TOWERDEFENSE_API AGhostTower_Base : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGhostTower_Base();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class ATowerBase> TowerClassReference;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USphereComponent* Range;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<UStaticMeshComponent*> TowerMeshes;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UMaterial* RedMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterial* GreenMaterial;

	UFUNCTION(BlueprintCallable)
	void ChangeMeshMaterial(UMaterial* newMaterial);
	
public:
	UFUNCTION()
	void OnOverlapEnter(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnOverlapExit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};
