// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "TowerDataAsset.h"
#include "GameFramework/Actor.h"
#include "TowerBase.generated.h"

UCLASS()
class TOWERDEFENSE_API ATowerBase : public AActor
{
	GENERATED_BODY()
	
public:	
	ATowerBase();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere,Category="ATOWERBASE")
	class UTowerDataAsset* DataAsset;

	UPROPERTY(BlueprintReadOnly)
	int32 Level;

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly, Category="ATOWERBASE")
	UStaticMeshComponent* BaseMesh;
	
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly, Category="ATOWERBASE")
	class USphereComponent* ZoneSphere;

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly, Category="ATOWERBASE")
	float BehaviorInterval;

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly, Category="ATOWERBASE")
	float BehaviorValue;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="ATOWERBASE")
	class USoundBase* UpgradeSFX;
	
	FTimerHandle BehaviorTimer;

	FString Description;

	UFUNCTION(BlueprintCallable)
	void getNearbyEnemies(TArray<class AEnemyBase*>& NearbyEnemies);
	
	UFUNCTION(BlueprintCallable)
	void LookForTarget(AEnemyBase*& currentTarget, bool includeFlying);

	UFUNCTION(BlueprintCallable)
	virtual FHitResult AimAndShootAtEnemy(class AEnemyBase* Target, class USceneComponent* Pivot,class UArrowComponent* Muzzle, TSubclassOf<class AProjectileBase> ProjectileClass);

	class UBuffComponent* ApplyAndRefreshBuff(AActor* actor, TSubclassOf<class UBuffComponent> BuffClass);
	
	UFUNCTION(BlueprintCallable)
	virtual void TowerBehavior();

	UFUNCTION(BlueprintCallable)
	virtual void UpgradeStats();
	
	UFUNCTION(BlueprintCallable)
	virtual void UpgradeMesh();

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
public:	
	virtual void Tick(float DeltaTime) override;

	FORCEINLINE void setBehaviorInterval(float newInterval) { BehaviorInterval=newInterval;}

	UFUNCTION(BlueprintPure,BlueprintCallable)
	FORCEINLINE float getBehaviorInterval() { return BehaviorInterval; }

	UFUNCTION(BlueprintPure,BlueprintCallable)
	FORCEINLINE class UTowerDataAsset* getDataAsset() { return DataAsset; }
	
	UFUNCTION(BlueprintPure, BlueprintCallable)
	float getBehaviorValue();
	
	UFUNCTION(BlueprintPure, BlueprintCallable)
	int32 getSellValue();
	
	UFUNCTION(BlueprintPure, BlueprintCallable)
	FORCEINLINE int32 getLevel() { return Level; }
	
	UFUNCTION(BlueprintPure,BlueprintCallable)
	FORCEINLINE bool IsMaxLevel() { return Level >= DataAsset->LevelProperties.Num(); }

	UFUNCTION(BlueprintPure,BlueprintCallable)
	FORCEINLINE FString getDescription() { return Description; }
	
	UFUNCTION(BlueprintCallable)
	void OnUpgrade();
};

