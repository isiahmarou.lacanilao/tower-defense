// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower_LaserTurret.h"
#include "EnemyBase.h"
#include "Buff_Burn.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Components/ArrowComponent.h"

ATower_LaserTurret::ATower_LaserTurret()
{
	Pivot = CreateDefaultSubobject<USceneComponent>(TEXT("Pivot"));
	GunMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GunMesh"));
	Muzzle = CreateDefaultSubobject<UArrowComponent>("Muzzle");
	Pivot->SetupAttachment(RootComponent);
	GunMesh->SetupAttachment(Pivot);
	Muzzle->SetupAttachment(GunMesh);
	
}

void ATower_LaserTurret::BeginPlay()
{
	ATowerBase::BeginPlay();
	const FTimerDelegate delegate = FTimerDelegate::CreateUObject
			(this, &ATower_LaserTurret::TowerBehavior);

	GetWorld()->GetTimerManager().SetTimer(BehaviorTimer, delegate,BehaviorInterval,true,0.1f);
}

void ATower_LaserTurret::TowerBehavior()
{
	LookForTarget(currentTarget,true);
	
	if (IsValid(currentTarget))
	{
		FHitResult hit = AimAndShootAtEnemy(currentTarget,Pivot,Muzzle, LaserClass);

		if(laserSFX)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(),laserSFX,this->GetActorLocation(),this->GetActorRotation(), .2f);
		}
		if(AEnemyBase* enemy = Cast<AEnemyBase>(hit.Actor))
		{
			ApplyAndRefreshBuff(enemy, UBuff_Burn::StaticClass());
		}
	}
}

void ATower_LaserTurret::UpgradeStats()
{
	int i = Level-1;
	BehaviorInterval = DataAsset->LevelProperties[i].Interval;
	BehaviorValue = DataAsset->LevelProperties[i].Value;
	ZoneSphere->SetSphereRadius(DataAsset->LevelProperties[i].RadiusRange);
	

	Description.Empty();
	Description.Append("Burn Damage: " + FString::SanitizeFloat(BehaviorValue) + LINE_TERMINATOR);
	Description.Append("Attack Speed: per "+FString::SanitizeFloat(BehaviorInterval) + " second" + LINE_TERMINATOR);
	Description.Append("Attack Radius: " + FString::SanitizeFloat(ZoneSphere->GetScaledSphereRadius())+ " Units");
}

void ATower_LaserTurret::UpgradeMesh()
{
	ATowerBase::UpgradeMesh();

	int i = Level-1;
	if(i <= DataAsset->LevelProperties.Num())
	{
		GunMesh->SetStaticMesh(DataAsset->LevelProperties[i].GunMesh);
	}
}
