// Fill out your copyright notice in the Description page of Project Settings.


#include "Buff_Burn.h"

#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"

UBuff_Burn::UBuff_Burn()
{
	Value = 10.f;
	Interval = 1.f;
	BuffTicks = 5;
}

void UBuff_Burn::BeginPlay()
{
	Super::BeginPlay();
	if(BurnGFX)
	{
		//Emitter = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BurnGFX, owner->GetActorLocation());
		Emitter = UGameplayStatics::SpawnEmitterAttached(BurnGFX,owner->GetRootComponent());
		Emitter->AttachToComponent(owner->GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
	}

	UE_LOG(LogTemp, Warning, TEXT("Burn Active"));
	
}

void UBuff_Burn::BuffBehavior()
{
	owner->TakeDamage(Value,FDamageEvent(),nullptr,nullptr);
	currentTick--;
	if(currentTick<=0)
	{
		GetWorld()->GetTimerManager().ClearTimer(BuffTimerHandle);
		Emitter->DestroyComponent();
		this->DestroyComponent();
	}
}

void UBuff_Burn::setBurnGFX(UParticleSystem* Particle)
{
	BurnGFX = Particle;
}
