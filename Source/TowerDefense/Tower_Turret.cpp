// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower_Turret.h"
#include "Components/SphereComponent.h"
#include "EnemyBase.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Components/ArrowComponent.h"

ATower_Turret::ATower_Turret()
{	
	Pivot_ = CreateDefaultSubobject<USceneComponent>(TEXT("Pivot"));
	GunMesh_ = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GunMesh"));
	Muzzle_ = CreateDefaultSubobject<UArrowComponent>("Muzzle");
	
	Pivot_->SetupAttachment(RootComponent);
	GunMesh_->SetupAttachment(Pivot_);
	Muzzle_->SetupAttachment(GunMesh_);
	
	Description.Append("Damage : " + FString::SanitizeFloat(BehaviorValue) + LINE_TERMINATOR);
	Description.Append("Attack Speed: per"+FString::SanitizeFloat(BehaviorInterval) + " second");
}

void ATower_Turret::BeginPlay()
{
	ATowerBase::BeginPlay();

	const FTimerDelegate delegate = FTimerDelegate::CreateUObject
					(this, &ATower_Turret::TowerBehavior);

	GetWorld()->GetTimerManager().SetTimer(BehaviorTimer, delegate,BehaviorInterval,true,0.1f);
}

void ATower_Turret::TowerBehavior()
{
	LookForTarget(currentTarget,true);
	
	if (IsValid(currentTarget))
	{
		FHitResult hit = AimAndShootAtEnemy(currentTarget,Pivot_,Muzzle_, BulletClass);
		
		if(turretSFX)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(),turretSFX,this->GetActorLocation(),this->GetActorRotation(), 0.2f, 0.5f);
		}
		
		if(AEnemyBase* enemy = Cast<AEnemyBase>(hit.Actor))
		{
			enemy->TakeDamage(BehaviorValue,FDamageEvent(),nullptr,nullptr);
		}
	}
	else
	{
		Pivot_->SetRelativeRotation(FRotator(0.f,0.f,0.f));
	}
}

void ATower_Turret::UpgradeStats()
{
	int i = Level-1;
	BehaviorInterval = DataAsset->LevelProperties[i].Interval;
	BehaviorValue = DataAsset->LevelProperties[i].Value;
	ZoneSphere->SetSphereRadius(DataAsset->LevelProperties[i].RadiusRange);

	
	
	Description.Empty();
	Description.Append("Damage : " + FString::SanitizeFloat(BehaviorValue) + LINE_TERMINATOR);
	Description.Append("Attack Speed: per "+FString::SanitizeFloat(BehaviorInterval) + " second" + LINE_TERMINATOR);
	Description.Append("Attack Radius: " + FString::SanitizeFloat(ZoneSphere->GetScaledSphereRadius())+ " Units");
}

void ATower_Turret::UpgradeMesh()
{
	ATowerBase::UpgradeMesh();

	int i = Level-1;
	if(i <= DataAsset->LevelProperties.Num())
	{
		GunMesh_->SetStaticMesh(DataAsset->LevelProperties[i].GunMesh);
	}
}
