// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildablePlatform_Base.generated.h"

UCLASS()
class TOWERDEFENSE_API ABuildablePlatform_Base : public AActor
{
	GENERATED_BODY()
	
public:	
	ABuildablePlatform_Base();

protected:
	virtual void BeginPlay() override;
	
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly)
	class USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UStaticMeshComponent* PlatformMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UBoxComponent* BoxCollision;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class ATowerBase* TowerOnPlatform;
public:	
	FORCEINLINE void setTowerOnPlatform(ATowerBase* Tower) { TowerOnPlatform = Tower; }

	UFUNCTION(BlueprintPure,BlueprintCallable)
	FORCEINLINE FRotator getMeshComponentRotation() { return PlatformMesh->GetComponentRotation(); }
	
	bool IsBuildable();
};
