// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuffComponent.h"
#include "Buff_Overload.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API UBuff_Overload : public UBuffComponent
{
	GENERATED_BODY()
public:
	UBuff_Overload();
	
protected:
	virtual void BeginPlay() override;

	virtual void BuffBehavior() override;

	class ATowerBase* tower;
	
	float ownerInterval;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
};
