// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Base_CharacterPawn.h"
#include "EnemyBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnEnemyDies, AEnemyBase*, EnemyObj, bool, isKilledByPlayer);

UCLASS()
class TOWERDEFENSE_API AEnemyBase : public ABase_CharacterPawn
{
	GENERATED_BODY()
public:
	AEnemyBase();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UFloatingPawnMovement* EnemyMovement;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UHealthComponent* EnemyHealthComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString mName;

	UPROPERTY(EditAnywhere)
	float WeaponDamage;

	UPROPERTY(EditAnywhere)
	float MoveSpeed;

	UPROPERTY(EditAnywhere)
	bool isFlying;

	UPROPERTY(EditAnywhere)
	bool isBoss;

	UPROPERTY(EditAnywhere)
	int GoldDrop;

	UPROPERTY(EditAnywhere)
	class USoundBase* DeathSound;
	
	UPROPERTY(EditAnywhere)
	class USoundBase* AttackSound;
	
	UFUNCTION()
	void OnActorOverlap(AActor* OverlappedActor, AActor* OtherActor);

	virtual void OnDeath() override;

public:
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintPure, BlueprintCallable)
	FORCEINLINE	UHealthComponent* getEnemyHealthComponent() {	return EnemyHealthComponent;	}

	UFUNCTION(BlueprintPure, BlueprintCallable)
	FORCEINLINE FString getEnemyName() { return mName; }

	UFUNCTION(BlueprintPure, BlueprintCallable)
	FORCEINLINE float getMoveSpeed() { return MoveSpeed; }

	UFUNCTION(BlueprintPure, BlueprintCallable)
	FORCEINLINE bool getIsFlying() { return isFlying; }

	UFUNCTION(BlueprintPure, BlueprintCallable)
	FORCEINLINE bool getIsBoss() { return isBoss; }

	UFUNCTION(BlueprintPure, BlueprintCallable)
	FORCEINLINE int getGoldDrop() { return GoldDrop; }

	UFUNCTION(BlueprintCallable)
	void setMoveSpeed(float newSpeed);

	FOnEnemyDies OnEnemyDies;
};
