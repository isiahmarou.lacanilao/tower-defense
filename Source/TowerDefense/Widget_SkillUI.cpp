// Fill out your copyright notice in the Description page of Project Settings.


#include "Widget_SkillUI.h"

#include "Tower_SW_Nanobot.h"
#include "Components/ProgressBar.h"
#include "Components/Button.h"
#include "Tower_SW_Napalm.h"
#include "Tower_SW_Nuke.h"
#include "Components/TextBlock.h"

bool UWidget_SkillUI::Initialize()
{
	Super::Initialize();
	Napalm_BTN->SetVisibility(ESlateVisibility::Hidden);
	Nuke_BTN->SetVisibility(ESlateVisibility::Hidden);
	Nanobot_BTN->SetVisibility(ESlateVisibility::Hidden);
	Napalm_HintText->SetVisibility(ESlateVisibility::Hidden);
	return true;
}

void UWidget_SkillUI::setDisableButton(TSubclassOf<ATower_SuperWeaponBase> TowerClass)
{
	if(TowerClass->IsChildOf(ATower_SW_Napalm::StaticClass()))
	{
		Napalm_BTN->SetIsEnabled(false);
	}
	else if (TowerClass->IsChildOf(ATower_SW_Nuke::StaticClass()))
	{
		Nuke_BTN->SetIsEnabled(false);
	}
	
}

void UWidget_SkillUI::setEnableButton(TSubclassOf<ATower_SuperWeaponBase> TowerClass)
{
	if(TowerClass->IsChildOf(ATower_SW_Napalm::StaticClass()))
	{
		Napalm_BTN->SetIsEnabled(true);
	}
	else if (TowerClass->IsChildOf(ATower_SW_Nuke::StaticClass()))
	{
		Nuke_BTN->SetIsEnabled(true);
	}
	
}

void UWidget_SkillUI::OnTowerDestroyed(TSubclassOf<ATower_SuperWeaponBase> TowerClass)
{
	if(TowerClass->IsChildOf(ATower_SW_Napalm::StaticClass()))
	{
		NapalmTower = nullptr;
		Napalm_BTN->SetVisibility(ESlateVisibility::Hidden);
		Napalm_BTN->OnClicked.RemoveDynamic(this, &UWidget_SkillUI::onNapalmBTNClicked);
		Napalm_CB->PercentDelegate.Unbind();
	}
	else if (TowerClass->IsChildOf(ATower_SW_Nuke::StaticClass()))
	{
		NukeTower = nullptr;
		Nuke_BTN->SetVisibility(ESlateVisibility::Hidden);
		Nuke_BTN->OnClicked.RemoveDynamic(this, &UWidget_SkillUI::onNapalmBTNClicked);
		Nuke_CB->PercentDelegate.Unbind();
	}
}

void UWidget_SkillUI::onNapalmBTNClicked()
{
	if(NapalmTower)
	{
		NapalmTower->OnSkillActivate();

		Napalm_HintText->SetVisibility(ESlateVisibility::Visible);
		FTimerDelegate HideDelegate = FTimerDelegate::CreateUObject(Napalm_HintText, &UTextBlock::SetVisibility, ESlateVisibility::Hidden);
		FTimerHandle TimeHandle;
		GetWorld()->GetTimerManager().SetTimer(TimeHandle, HideDelegate, 3.f, false);
	}
	else
	{
		Napalm_BTN->SetIsEnabled(false);
	}
}

void UWidget_SkillUI::onNukeBTNClicked()
{
	if(NukeTower)
	{
		NukeTower->OnSkillActivate();
	}
	else
	{
		Napalm_BTN->SetIsEnabled(false);
	}
}

void UWidget_SkillUI::HideNapalmHint()
{
}

void UWidget_SkillUI::InitBTN(ATower_SuperWeaponBase* SuperTower)
{
	
	if(SuperTower->GetClass()->IsChildOf(ATower_SW_Napalm::StaticClass()))
	{
		NapalmTower = Cast<ATower_SW_Napalm>(SuperTower);
		
		NapalmTower->CoolDownStarts.AddUniqueDynamic(this, &UWidget_SkillUI::setDisableButton);
		NapalmTower->CoolDownEnds.AddUniqueDynamic(this, &UWidget_SkillUI::setEnableButton);
		NapalmTower->TowerSold.AddUniqueDynamic(this, &UWidget_SkillUI::OnTowerDestroyed);
		
		Napalm_BTN->SetVisibility(ESlateVisibility::Visible);
		Napalm_BTN->SetIsEnabled(true);
		Napalm_BTN->OnClicked.AddDynamic(this, &UWidget_SkillUI::onNapalmBTNClicked);
	
		Napalm_CB->PercentDelegate.BindUFunction(NapalmTower, "getCountDownPercentage");
		Napalm_CB->SynchronizeProperties();
	}
	if(SuperTower->GetClass()->IsChildOf(ATower_SW_Nuke::StaticClass()))
	{	
		NukeTower = Cast<ATower_SW_Nuke>(SuperTower);

		NukeTower->CoolDownStarts.AddUniqueDynamic(this, &UWidget_SkillUI::setDisableButton);
		NukeTower->CoolDownEnds.AddUniqueDynamic(this, &UWidget_SkillUI::setEnableButton);
		NukeTower->TowerSold.AddUniqueDynamic(this, &UWidget_SkillUI::OnTowerDestroyed);

		Nuke_BTN->SetVisibility(ESlateVisibility::Visible);
		Nuke_BTN->SetIsEnabled(true);
		Nuke_BTN->OnClicked.AddDynamic(this, &UWidget_SkillUI::onNukeBTNClicked);

		Nuke_CB->PercentDelegate.BindUFunction(NukeTower, "getCountDownPercentage");
		Nuke_CB->SynchronizeProperties();
	}
	if(SuperTower->GetClass()->IsChildOf(ATower_SW_Nanobot::StaticClass()))
	{
		Nanobot_BTN->SetVisibility(ESlateVisibility::Visible);
		Nanobot_BTN->SetIsEnabled(false);
	}
}
