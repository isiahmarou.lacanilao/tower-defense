// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuGM.h"

#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"

AMainMenuGM::AMainMenuGM()
{
}

void AMainMenuGM::BeginPlay()
{
	APlayerController* pc = UGameplayStatics::GetPlayerController(GetWorld(),0);

	UGameplayStatics::SetGamePaused(GetWorld(),true);
	pc->SetShowMouseCursor(true);

	if(MainMenuBGM)
	{
		UGameplayStatics::PlaySound2D(GetWorld(),MainMenuBGM,0.5f);
	}

	UUserWidget* mainMenu =  CreateWidget<UUserWidget>(GetWorld(), MainMenuUI);
	if(mainMenu)
	{
		mainMenu->AddToViewport(1);
	}
}
