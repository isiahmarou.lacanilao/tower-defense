// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower_SuperWeaponBase.h"
#include "Tower_SW_Nuke.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATower_SW_Nuke : public ATower_SuperWeaponBase
{
	GENERATED_BODY()
public:
	ATower_SW_Nuke();
	
protected:
	virtual void BeginPlay() override;

	virtual void UpgradeStats() override;

	class ATowerDefenseGameModeBase* GameMode;
	
public:
	virtual void OnSkillActivate() override;
};
