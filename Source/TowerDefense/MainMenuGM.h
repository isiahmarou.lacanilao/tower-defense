// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MainMenuGM.generated.h"

UCLASS()
class TOWERDEFENSE_API AMainMenuGM : public AGameModeBase
{
	GENERATED_BODY()
public:
	AMainMenuGM();
protected:
	void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	class USoundBase* MainMenuBGM;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> MainMenuUI;
};
