// Fill out your copyright notice in the Description page of Project Settings.


#include "Base_CharacterPawn.h"
#include "HealthComponent.h"

// Sets default values
ABase_CharacterPawn::ABase_CharacterPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	HealthComponent = CreateDefaultSubobject<UHealthComponent>("HealthComponent");
}

// Called when the game starts or when spawned
void ABase_CharacterPawn::BeginPlay()
{
	Super::BeginPlay();
}

void ABase_CharacterPawn::OnDeath()
{
}

// Called every frame
void ABase_CharacterPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABase_CharacterPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

