// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyController.h"
#include "EnemyBase.h"

void AEnemyController::BeginPlay()
{
	Super::BeginPlay();
	owner = Cast<AEnemyBase>(this->GetPawn());
}

void AEnemyController::MoveToWaypoint()
{
	if (Waypoints.Num() <= 0 ) return;
	if (currentWaypoint <= 0 ) return;

	if (currentWaypoint <= Waypoints.Num())
	{
		for (int i = 0; i < Waypoints.Num(); i++)
		{
			if (AWaypoint* WP_Ptr = Cast<AWaypoint>(Waypoints[i]))
			{
				if (WP_Ptr->getOrder() == currentWaypoint)
				{
					MoveToActor(WP_Ptr);
					currentWaypoint++;
					break;
				}
			}
		}
	}
}

void AEnemyController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	MoveToWaypoint();
}

void AEnemyController::GoToCore(TArray<AWaypoint*> LanePath)
{
	Waypoints = LanePath;
	currentWaypoint = 1;
	MoveToWaypoint();
}


