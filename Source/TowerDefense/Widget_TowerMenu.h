// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "BuildManager.h"
#include "Blueprint/UserWidget.h"

#include "Widget_TowerMenu.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API UWidget_TowerMenu : public UUserWidget
{
	GENERATED_BODY()

	virtual bool Initialize();

protected:
	UPROPERTY(meta = (BindWidget))
	class UCanvasPanel* TowerMenu_Panel;
	
	UPROPERTY(meta = (BindWidget))
	class  UButton* Upgrade_BTN;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* Upgrade_TXT;
	
	UPROPERTY(meta = (BindWidget))
	UButton* Sell_BTN;
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* Sell_TXT;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* Name_TXT;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* Level_TXT;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* Description_TXT;

	class ATowerBase* Tower;

	class ABuildManager* BuildManager;

	class APlayerPawn* player;
	
	UFUNCTION()
	void OnVisibilityChange(ESlateVisibility InVisibility);

	void ChangeInfos();
	
	UFUNCTION()
	void OnUpgradeBTNClicked();

	UFUNCTION()
	void OnSellBTNClicked();

public:
	FORCEINLINE void setTower(class ATowerBase* _Tower) { Tower = _Tower; }
	
	FORCEINLINE void setBuildManager(class ABuildManager* _BuildManager) { BuildManager = _BuildManager; }
	
	FORCEINLINE void setPlayer(class APlayerPawn* _player) { player = _player; }
};
