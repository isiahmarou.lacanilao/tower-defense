// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnerEnemy.generated.h"

UENUM(BlueprintType)
enum class LaneToFollow : uint8 {
	LEFT = 0 UMETA(DisplayName = "LEFT"),
	RIGHT = 1  UMETA(DisplayName = "RIGHT"),
	MIDDLE = 2     UMETA(DisplayName = "MIDDLE"),
};

UCLASS()
class TOWERDEFENSE_API ASpawnerEnemy : public AActor
{
	GENERATED_BODY()
	
public:	
	ASpawnerEnemy();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UBoxComponent* SpawnArea;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	LaneToFollow mLane;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<class AWaypoint*> LeftLane_Waypoints;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<AWaypoint*> RightLane_Waypoints;

	void giveLaneWaypointsToEnemy(class AEnemyController* eController);

public:	
	UFUNCTION(BlueprintCallable)
	class AEnemyBase* InitEnemy(TSubclassOf<class AEnemyBase> EnemyClass);
};
