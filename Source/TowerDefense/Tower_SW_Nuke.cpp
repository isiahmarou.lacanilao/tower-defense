// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower_SW_Nuke.h"

#include "Kismet/GameplayStatics.h"
#include "TowerDefenseGameModeBase.h"
#include "EnemyBase.h"

ATower_SW_Nuke::ATower_SW_Nuke()
{
	
}

void ATower_SW_Nuke::BeginPlay()
{
	ATower_SuperWeaponBase::BeginPlay();
	if(UWorld* world = GetWorld())
	{
		GameMode = Cast<ATowerDefenseGameModeBase>(UGameplayStatics::GetGameMode(world));
	}
	//OnSkillActivate();
}

void ATower_SW_Nuke::UpgradeStats()
{
	int i = Level-1;
	BehaviorInterval = DataAsset->LevelProperties[i].Interval;
	BehaviorValue = DataAsset->LevelProperties[i].Value;
	
	Description.Empty();
	Description.Append("Grants the Nuke Skill" LINE_TERMINATOR);
	Description.Append("Nuke Skill kill all enemies on active");
}

void ATower_SW_Nuke::OnSkillActivate()
{
	if(!IsOnCoolDown())
	{

	}
	TArray<AEnemyBase*> Enemies;
	GameMode->getEnemies(Enemies);

	for(AEnemyBase* enemy : Enemies)
	{
		if(!enemy->getIsBoss())
		{
			enemy->TakeDamage(9999, FDamageEvent(), nullptr,nullptr);
		}
	}

	this->CoolDownStarts.Broadcast(this->GetClass());
	CountDown = BehaviorInterval;
		
	const FTimerDelegate delegate = FTimerDelegate::CreateUObject
				(this, &ATower_SW_Nuke::OnCountDown);
		
	GetWorld()->GetTimerManager().SetTimer(BehaviorTimer, delegate,1.f,true);
}
