// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "Widget_SkillUI.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API UWidget_SkillUI : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual bool Initialize() override;
protected:
	//NAPALM PROPERTIES
	UPROPERTY(meta = (BindWidget))
	class UButton* Napalm_BTN;

	UPROPERTY(meta = (BindWidget))
	class UProgressBar* Napalm_CB;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* Napalm_HintText;
	
	class ATower_SW_Napalm* NapalmTower;
	
	//NUKE PROPERTIES
	UPROPERTY(meta = (BindWidget))
	class UButton* Nuke_BTN;

	UPROPERTY(meta = (BindWidget))
	class UProgressBar* Nuke_CB;
	
	class ATower_SW_Nuke* NukeTower;
	
	//NANOBOT PROPERTIES
	UPROPERTY(meta = (BindWidget))
	class UButton* Nanobot_BTN;


	//FUNCTIONS
	UFUNCTION()
	void setDisableButton(TSubclassOf<class ATower_SuperWeaponBase> TowerClass);

	UFUNCTION()
	void setEnableButton(TSubclassOf<class ATower_SuperWeaponBase> TowerClass);

	UFUNCTION()
	void OnTowerDestroyed(TSubclassOf<class ATower_SuperWeaponBase> TowerClass);

	UFUNCTION()
	void onNapalmBTNClicked();

	UFUNCTION()
	void onNukeBTNClicked();

	UFUNCTION()
	void HideNapalmHint();
public:
	void InitBTN(ATower_SuperWeaponBase* SuperTower);

	
};
