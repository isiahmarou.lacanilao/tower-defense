// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TowerBase.h"
#include "Tower_MissileLauncher.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATower_MissileLauncher : public ATowerBase
{
	GENERATED_BODY()
public:
	ATower_MissileLauncher();
protected:
	virtual void BeginPlay() override;
	
	virtual void TowerBehavior() override;
	
	virtual FHitResult AimAndShootAtEnemy(class AEnemyBase* Target, USceneComponent* Pivot, UArrowComponent* Muzzle, TSubclassOf<class AProjectileBase> ProjectileClass) override;

	void MissileImpact(FHitResult OutHit);

	UPROPERTY(EditAnywhere,BlueprintReadOnly)
	UStaticMeshComponent* GunMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="ATOWERBASE")
	class USceneComponent* Pivot_;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="ATOWERBASE")
	class UArrowComponent* Muzzle_;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="ATower_MissileLauncher")
	float explosionRadius;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="ATower_MissileLauncher")
	TSubclassOf<class AProjectileBase> MissileClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="ATower_MissileLauncher")
	class USoundBase* MissileLaunchSFX;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="ATower_MissileLauncher")
	class USoundBase* explosionSFX;
	
	AEnemyBase* currentTarget;
	
	virtual void UpgradeStats() override;

	virtual void UpgradeMesh() override;

};
