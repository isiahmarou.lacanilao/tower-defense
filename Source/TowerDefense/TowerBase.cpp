
#include "TowerBase.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/ArrowComponent.h"
#include "EnemyBase.h"
#include "Kismet/GameplayStatics.h"
#include "ProjectileBase.h"
#include "DrawDebugHelpers.h"
#include "Components/SphereComponent.h"
#include "BuffComponent.h"


ATowerBase::ATowerBase()
{
	PrimaryActorTick.bCanEverTick = true;
	RootComponent= CreateDefaultSubobject<USceneComponent>(TEXT("RootScene"));
	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	
	BaseMesh->SetupAttachment(RootComponent);
	BaseMesh->SetRelativeRotation(FRotator(0,-90.f,0));
	ZoneSphere = CreateDefaultSubobject<USphereComponent>(TEXT("ZoneSphere"));
	ZoneSphere->SetupAttachment(BaseMesh);
}

void ATowerBase::BeginPlay()
{
	Super::BeginPlay();
	Level = 0;
	OnUpgrade();
}

void ATowerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


void ATowerBase::getNearbyEnemies(TArray<AEnemyBase*>& NearbyEnemies)
{
	NearbyEnemies.Empty();
	
	TArray<AActor*> Actors_Arr;
	ZoneSphere->GetOverlappingActors(Actors_Arr, AEnemyBase::StaticClass());

	for(AActor* actor : Actors_Arr)
	{
		if(AEnemyBase* enemy = Cast<AEnemyBase>(actor))
		{
			NearbyEnemies.Add(enemy);
		}
	}
}

void ATowerBase::LookForTarget(AEnemyBase*& currentTarget, bool includeFlying)
{
	TArray<AEnemyBase*> NearbyEnemies;
	
	getNearbyEnemies(NearbyEnemies);

	// check if current target is still in range
	for(AEnemyBase* enemy : NearbyEnemies)
	{
		if(enemy == currentTarget)
		{
			if(includeFlying) return;
			
			// doesn't include lying enemies
			if(!enemy->getIsFlying()) return;
		}
	}
	
	// if current is not in range, target the first valid enemy
	for(AEnemyBase* enemy : NearbyEnemies)
	{
		if(includeFlying)
		{
			currentTarget = enemy;
			return;
		}

		if(!enemy->getIsFlying())
		{
			currentTarget = enemy;
			return;
		}
	}
	
	// if there is no target, return set current Target to nullptr
	currentTarget = nullptr;
}

FHitResult ATowerBase::AimAndShootAtEnemy(AEnemyBase* Target, USceneComponent* Pivot, UArrowComponent* Muzzle, TSubclassOf<AProjectileBase> ProjectileClass)
{
	FHitResult OutHit;
	
	if(!IsValid(Target)) return OutHit;

	// Aim At Target
	
	FVector targetLoc =  Target->GetPawnViewLocation();
	FRotator lookAtRot = UKismetMathLibrary::FindLookAtRotation(Pivot->GetComponentLocation(), targetLoc);
	Pivot->SetWorldRotation(lookAtRot);


	// Shoot At Target
	FVector Start = Muzzle->GetComponentLocation();
	FVector ForwardVector = Muzzle->GetForwardVector();
	FVector End = ((ForwardVector * 1700.f) + Start);
	FCollisionQueryParams CollisionParams;

	if(UWorld* world = GetWorld())
	{
		world->LineTraceSingleByChannel(OutHit, Start, End, ECC_Visibility, FCollisionQueryParams());
	}
	
	// For debug must delete
	//DrawDebugLine(GetWorld(), Start, OutHit.Location, FColor::Green, true, 3.0f);

	//Spawn projectile
	if (UWorld* world = GetWorld())
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	
		AProjectileBase* projectile = world->SpawnActor<AProjectileBase>(ProjectileClass, Muzzle->GetComponentTransform(), SpawnParameters);
	}
	return OutHit;
}

class UBuffComponent* ATowerBase::ApplyAndRefreshBuff(AActor* actor, TSubclassOf<UBuffComponent> BuffClass)
{
	if(UBuffComponent* BuffComponent = Cast<UBuffComponent>(actor->GetComponentByClass(BuffClass)))
	{
		BuffComponent->setBuffValue(BehaviorValue);
		BuffComponent->RefreshBuff();
		return nullptr;
	}
	else
	{
		UBuffComponent* buff = NewObject<UBuffComponent>(actor,BuffClass);
		buff->setBuffValue(BehaviorValue);
		actor->AddOwnedComponent(buff);
		buff->RegisterComponent();
		return buff;
	}
}

void ATowerBase::TowerBehavior()
{
	
}

void ATowerBase::UpgradeStats()
{
	
}

void ATowerBase::UpgradeMesh()
{
	int i = Level-1;
	if(i <= DataAsset->LevelProperties.Num())
	{
		BaseMesh->SetStaticMesh(DataAsset->LevelProperties[i].BaseMesh);
	}
}

float ATowerBase::getBehaviorValue()
{
	return BehaviorValue;
}

int32 ATowerBase::getSellValue()
{
	int sellvalue = 0;

	for(int32 i = 0; i < Level; i++)
	{
		sellvalue+=DataAsset->LevelProperties[i].Cost;
	}
	
	return sellvalue/2;
}

void ATowerBase::OnUpgrade()
{
	if(this->IsMaxLevel()) return;
	Level++;
	UpgradeStats();
	UpgradeMesh();

	if(UpgradeSFX)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(),UpgradeSFX,this->GetActorLocation(),this->GetActorRotation(),.3f);
	}
}

void ATowerBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if(GetWorld()->GetTimerManager().IsTimerActive(BehaviorTimer))
	{
		GetWorldTimerManager().ClearTimer(BehaviorTimer);
	}
}
