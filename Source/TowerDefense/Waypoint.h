// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TargetPoint.h"
#include "Waypoint.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AWaypoint : public ATargetPoint
{
	GENERATED_BODY()
private:
	UPROPERTY(EditAnywhere)
	int LaneNumber;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	int Order;
public:
	int getLaneNumber();

	int getOrder();
};
