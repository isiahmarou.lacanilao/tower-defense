// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TowerDefenseGameModeBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWaveEnds, class ATowerDefenseGameModeBase*, GameMode);
UCLASS()
class TOWERDEFENSE_API ATowerDefenseGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly)
	TArray<class AEnemyBase*> Enemies_Arr;

	UPROPERTY(BlueprintReadOnly)
	class APlayerPawn* mPlayerPawn;

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	TArray<class ASpawnerEnemy*> Spawners_Arr;

	UPROPERTY(EditAnywhere,BlueprintReadOnly, Category = "Wave Data Assets")
	TArray<class UWaveDataAsset*> WavesDA_Arr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class USoundBase* BackgroundMusic;
	
	UPROPERTY(BlueprintReadWrite)
	FTimerHandle CountDownTimerHandle;

	UPROPERTY(BlueprintReadOnly)
	int32 TotalNumToSpawn;

	UPROPERTY(BlueprintReadOnly)
	float SpawnInterval;

	UPROPERTY(BlueprintReadWrite)
	FTimerHandle SpawnEnemyTimerHandle;

	UPROPERTY(BlueprintReadOnly)
	int32 WaveNum;

	UPROPERTY(BlueprintReadOnly)
	bool isWaveInProgress;

	UFUNCTION(BlueprintCallable)
	void SpawnWave(UWaveDataAsset* WaveDA);

	int32 KillCount;

	int32 currentNumToSpawn;
	
	UWaveDataAsset* currentWaveDA;
public:
	UFUNCTION(BlueprintCallable)
	void OnTowerDefense();
	
	UFUNCTION(BlueprintPure, BlueprintCallable)
	FORCEINLINE float getTotalNumToSpawn() { return TotalNumToSpawn; }

	UFUNCTION(BlueprintPure, BlueprintCallable)
	FORCEINLINE float getCurrentNumToSpawn() { return currentNumToSpawn; }

	UFUNCTION(BlueprintPure, BlueprintCallable)
	FORCEINLINE int getCurrentWaveNumber() { return WaveNum; }

	UFUNCTION(BlueprintPure, BlueprintCallable)
	FORCEINLINE int32 getKillCount() { return KillCount; }

	FORCEINLINE void getEnemies(TArray<class AEnemyBase*>& Array_Ref) { Array_Ref = Enemies_Arr; }

	UFUNCTION()
	void OnEnemyDeath(class AEnemyBase* enemy, bool isKilledByPlayer);

	UPROPERTY(BlueprintAssignable, category = "EventDispatchers")
	FOnWaveEnds WaveEnds;

	UFUNCTION(BlueprintImplementableEvent)
	void OnGameEnds(bool playerWon);
};
