// Fill out your copyright notice in the Description page of Project Settings.


#include "Buff_Overload.h"
#include "TowerBase.h"

UBuff_Overload::UBuff_Overload()
{
	Value = 10.f;
	Interval = 1.f;
	BuffTicks = 3;
}

void UBuff_Overload::BeginPlay()
{
	Super::BeginPlay();
	tower = Cast<ATowerBase>(owner);

	ownerInterval = tower->getBehaviorInterval();

	float temp = ownerInterval * Value;
	tower->setBehaviorInterval(ownerInterval-temp);

	UE_LOG(LogTemp, Warning, TEXT("Overload Active"));
}

void UBuff_Overload::BuffBehavior()
{
	float temp = ownerInterval * Value;
	tower->setBehaviorInterval(ownerInterval - temp);
	
	currentTick--;
	if(currentTick<=0)
	{
		GetWorld()->GetTimerManager().ClearTimer(BuffTimerHandle);
		
		tower->setBehaviorInterval(ownerInterval);
		
		DestroyComponent();
	}
}

void UBuff_Overload::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	tower->setBehaviorInterval(ownerInterval);
}