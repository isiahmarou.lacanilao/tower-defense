// Fill out your copyright notice in the Description page of Project Settings.


#include "Buff_Nanobot.h"

#include "HealthComponent.h"

UBuff_Nanobot::UBuff_Nanobot()
{
	Value = 0.1f;
	Interval = 0.1f;
	BuffTicks = 9999999;
}

void UBuff_Nanobot::BeginPlay()
{
	Super::BeginPlay();
	enemy = Cast<AEnemyBase>(GetOwner());
}

void UBuff_Nanobot::BuffBehavior()
{
	float HP_Percent = enemy->getEnemyHealthComponent()->getHealthPercentage();
	
	if(HP_Percent <= Value)
	{
		OnEnemyKill.Broadcast(enemy);
		enemy->TakeDamage(9999,FDamageEvent(), nullptr, nullptr);
	}
}
