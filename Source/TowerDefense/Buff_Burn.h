// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuffComponent.h"
#include "Buff_Burn.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API UBuff_Burn : public UBuffComponent
{
	GENERATED_BODY()
public:
	UBuff_Burn();
	
protected:
	UPROPERTY()
	class UParticleSystem* BurnGFX;
	
	virtual void BeginPlay() override;

	virtual void BuffBehavior() override;
	
	class UParticleSystemComponent* Emitter;
public:
	UFUNCTION(BlueprintCallable)
	void setBurnGFX(class UParticleSystem* Particle);
	
};
