#include "ProjectileBase.h"
#include "TowerBase.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"

AProjectileBase::AProjectileBase()
{
	PrimaryActorTick.bCanEverTick = true;
	
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>("ProjectileMovement");
	RootComponent= Mesh;
	LifeSpawn = 5.f;
}

void AProjectileBase::BeginPlay()
{
	Super::BeginPlay();
	Mesh->OnComponentHit.AddDynamic(this,  &AProjectileBase::OnHit);
	SetLifeSpan(LifeSpawn);
}

void AProjectileBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectileBase::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if(particle)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),particle,this->GetActorLocation(),this->GetActorRotation());
	}
	this->Destroy();
}


