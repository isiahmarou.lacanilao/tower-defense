

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildManager.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FBuildModeEnd, class ABuildManager*, BuildManager);
UCLASS()
class TOWERDEFENSE_API ABuildManager : public AActor
{
	GENERATED_BODY()
	
public:	
	ABuildManager();

protected:
	virtual void BeginPlay() override;

	UPROPERTY()
	class APlayerPawn* _PlayerPawn;

	UPROPERTY()
	class ATowerDefenseController* _Controller;

	UPROPERTY()
	class UTowerDataAsset* TowerToBuildData;

	UPROPERTY(EditAnywhere,BlueprintReadOnly)
	TArray<UTowerDataAsset*> TowerData;

	UPROPERTY(EditAnywhere,BlueprintReadOnly)
	class USoundBase* sellSFX;
	
	UPROPERTY()
	class AGhostTower_Base* currentGhostTower;

	UPROPERTY()
	TArray<class ATowerBase*> TowersBuilt;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool isInBuildMode;

	UPROPERTY(EditAnywhere,BlueprintReadOnly)
	TSubclassOf<class UUserWidget> ClassTowerMenu;

	class UWidget_TowerMenu* TowerMenuWidget;

	
public:	
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FTimerHandle ChangeLocationHandle;

	UFUNCTION(BlueprintPure, BlueprintCallable)
	FORCEINLINE bool getIsInBuildMode() { return isInBuildMode; }

	UFUNCTION(BlueprintCallable)
	bool OnBuildMode(class UTowerDataAsset* mTowerData);

	UFUNCTION(BlueprintCallable)
	void OnBuildModeEnd();

	UFUNCTION(BlueprintCallable)
	void BuildTowerOnPlatform(class ABuildablePlatform_Base* platform);
	
	UFUNCTION(BlueprintCallable)
	void OnTowerUpgrade(ATowerBase* tower);
	
	UFUNCTION(BlueprintCallable)
	void SellTower(ATowerBase* tower);

	UFUNCTION()
	void ShowTowerWidget(ATowerBase* tower);

	UPROPERTY(BlueprintAssignable)
	FBuildModeEnd BuildModeEnd;
};
