#include "PlayerCore.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerPawn.h"

// Sets default values
APlayerCore::APlayerCore()
{
	PrimaryActorTick.bCanEverTick = false;

}

void APlayerCore::BeginPlay()
{
	Super::BeginPlay();
	
	Player = Cast<APlayerPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));

}

// Called every frame
void APlayerCore::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlayerCore::damagePlayerHealth(float damage, APawn* DamageCauser)
{
	Player->TakeDamage(damage, FDamageEvent::FDamageEvent(), DamageCauser->GetController(), DamageCauser);
}

