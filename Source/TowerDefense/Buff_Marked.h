// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuffComponent.h"
#include "Buff_Marked.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnOwnerKilled);
UCLASS()
class TOWERDEFENSE_API UBuff_Marked : public UBuffComponent
{
	GENERATED_BODY()
public:
	UBuff_Marked();

	FOnOwnerKilled OwnerKilled;
protected:
	virtual void BeginPlay() override;

	virtual void BuffBehavior() override;

	class AEnemyBase* enemy;

	UFUNCTION()
	void OwnerDeath(class AEnemyBase* enemyRef, bool isKilledByPlayer);

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
};
