// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower_SuperWeaponBase.h"
#include "Tower_SW_Nanobot.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATower_SW_Nanobot : public ATower_SuperWeaponBase
{
	GENERATED_BODY()
public:
	ATower_SW_Nanobot();
	
protected:
	virtual void BeginPlay() override;

	virtual void UpgradeStats() override;

	class ATowerDefenseGameModeBase* GameMode;

	UFUNCTION()
	void DamageNearbyEnemies(AEnemyBase* enemy);

	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
public:
	virtual void OnSkillActivate() override;
};
