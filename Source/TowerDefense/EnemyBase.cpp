// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyBase.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "PlayerCore.h"
#include "Kismet/GameplayStatics.h"
#include "EnemyController.h"
#include "HealthComponent.h"

AEnemyBase::AEnemyBase()
{
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	RootComponent = SceneComponent;
	EnemyHealthComponent = HealthComponent;
	EnemyMovement = CreateDefaultSubobject<UFloatingPawnMovement>("EnemyMovement");

	WeaponDamage = 5;
	MoveSpeed = 300;
}

void AEnemyBase::BeginPlay()
{
	Super::BeginPlay();

	OnActorBeginOverlap.AddDynamic(this, &AEnemyBase::OnActorOverlap);

	setMoveSpeed(MoveSpeed);

	EnemyHealthComponent->OnPawnDeath.AddDynamic(this, &AEnemyBase::OnDeath);
}

void AEnemyBase::OnActorOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if (OtherActor->GetClass()->IsChildOf(APlayerCore::StaticClass()))
	{
		Cast<APlayerCore>(OtherActor)->damagePlayerHealth(WeaponDamage, this);
		OnEnemyDies.Broadcast(this, false);
		if(AttackSound)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(),AttackSound,GetActorLocation(),GetActorRotation(),0.1f);
		}
		this->Destroy();
	}
}

void AEnemyBase::OnDeath()
{
	ABase_CharacterPawn::OnDeath();

	if(DeathSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(),DeathSound,this->GetActorLocation(),this->GetActorRotation(),0.2f);
	}
	OnEnemyDies.Broadcast(this,true);
	this->Destroy();
}

void AEnemyBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AEnemyBase::setMoveSpeed(float newSpeed)
{
	MoveSpeed = newSpeed;
	EnemyMovement->MaxSpeed = MoveSpeed;
}
