// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"

UHealthComponent::UHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	maxHealth = 100;
	currentHealth = maxHealth;
	AActor* Owner = GetOwner();
	if (Owner)
	{
		Owner->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::TakeDamage);
	}
}

void UHealthComponent::TakeDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	currentHealth = FMath::Clamp(currentHealth - Damage, 0.0f, maxHealth);

	if (currentHealth <= 0)
	{
		OnPawnDeath.Broadcast();
	}
}

float UHealthComponent::getHealthPercentage()
{
	return currentHealth/maxHealth;
}

float UHealthComponent::getCurrentHealth()
{
	return currentHealth;
}

float UHealthComponent::getMaxHealth()
{
	return maxHealth;
}

void UHealthComponent::scaleMaxHealth(float valueScale)
{
	maxHealth *= valueScale;
	currentHealth = maxHealth;
}

