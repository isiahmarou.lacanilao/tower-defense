// Fill out your copyright notice in the Description page of Project Settings.


#include "Buff_Marked.h"
#include "EnemyBase.h"

UBuff_Marked::UBuff_Marked()
{
	Interval = 1.f;
	BuffTicks = 3;
}

void UBuff_Marked::BeginPlay()
{
	Super::BeginPlay();
	
	enemy = Cast<AEnemyBase>(owner);
	enemy->OnEnemyDies.AddUniqueDynamic(this, &UBuff_Marked::OwnerDeath);    

	UE_LOG(LogTemp, Warning, TEXT("marked Active") );
}

void UBuff_Marked::BuffBehavior()
{
	currentTick--;
	if(currentTick<=0)
	{
		GetWorld()->GetTimerManager().ClearTimer(BuffTimerHandle);
		DestroyComponent();
	}
}

void UBuff_Marked::OwnerDeath(AEnemyBase* enemyRef, bool isKilledByPlayer)
{
	if(isKilledByPlayer)
	{
		OwnerKilled.Broadcast();
	}
}

void UBuff_Marked::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	
}
