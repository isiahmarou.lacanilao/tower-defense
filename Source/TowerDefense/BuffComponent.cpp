// Fill out your copyright notice in the Description page of Project Settings.


#include "BuffComponent.h"

// Sets default values for this component's properties
UBuffComponent::UBuffComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}


// Called when the game starts
void UBuffComponent::BeginPlay()
{
	currentTick = BuffTicks;
	Super::BeginPlay();
	owner = GetOwner();
	const bool isRepeating = BuffTicks > 1;
	const FTimerDelegate delegate = FTimerDelegate::CreateUObject
				(this, &UBuffComponent::BuffBehavior);

	GetWorld()->GetTimerManager().SetTimer(BuffTimerHandle, delegate,Interval,isRepeating,0.1f);
}

void UBuffComponent::BuffBehavior()
{
	
}

// Called every frame
void UBuffComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UBuffComponent::RefreshBuff()
{
	currentTick = BuffTicks;
}

void UBuffComponent::setBuffValue(float newValue)
{
	Value = newValue;
}

void UBuffComponent::setBuffInterval(float newInterval)
{
	Interval = newInterval;
}

void UBuffComponent::setBuffTicks(int newTicks)
{
	BuffTicks = newTicks;
}



